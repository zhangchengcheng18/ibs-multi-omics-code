#Plot the metadata to show sample completeness
m_all$label <- paste(m_all$Timepoint, m_all$sType, sep="")
m_all$label <- factor(m_all$label, levels=c("1Biopsy", "0Stool","1Stool","2Stool","3Stool","4Stool","5Stool","6Stool", "2Biopsy", "FlareFlare"))
m_all$Flare <- as.character(m_all$Timepoint)
m_all$Flare[-which(m_all$Flare == "Flare")] <- "Normal"
m_all$Flare <- factor(m_all$Flare, levels = c("Normal", "Flare"))
m_all$SB <- as.character(m_all$sType)
m_all$SB[m_all$SB == "Flare"] <- "Stool"

#pdf("overview figure subject timelines.pdf", width=3.5, height=3.5)
png("overview figure subject timelines.png", width=3.5*300, height=2.5*300, res=300)
ggplot(m_all) +
  geom_tile(aes(x=label, y=factor(study_id), fill=Cohort), color="white") +
  facet_grid(Cohort ~ Flare, scales="free", space="free") +
  scale_x_discrete(labels=c("Biopsy","0", "1", "2", "3", "4", "5", "6", "Biopsy", "Flare")) +
  scale_fill_manual(values = sel_colors) +
  theme(axis.text.x=element_text(angle=45,hjust=1),
        axis.text.y =element_blank(),
        axis.ticks.y =element_blank(),
        strip.text.x = element_text(size = 8),
        strip.text.y = element_text(size = 8, angle=0),
        panel.background=element_blank()) +
  labs(y="", x="") +
  guides(fill=F, color=F, alpha=F)
dev.off()


#------------------------------------------------------------
all_study_ids <- unique(m_all$study_id)
#length(all_study_ids) #77

m_all_unique <- m_all[sapply(all_study_ids, function(x) {which(m_all$study_id == x)[[1]]}),]
n_cohort <-table(m_all_unique$Cohort)
ages <- aggregate(m_all_unique$Age, list(m_all_unique$Cohort), mean)$x
ages_sd <- aggregate(m_all_unique$Age, list(m_all_unique$Cohort), sd)$x
bhi <- aggregate(m_all_unique$BMI, list(m_all_unique$Cohort), FUN=mean, na.rm=T)$x #one NA
bhi_sd <- aggregate(m_all_unique$BMI, list(m_all_unique$Cohort), sd, na.rm=T)$x
t_sex <- table(m_all_unique$Gender, m_all_unique$Cohort)
n_female <- t_sex[1,]
perc_female <- prop.table(t_sex, 2)[1,] *100

 

biopsy_study_ids <- m_all$study_id[m_all$sType == "Biopsy"]
n_biopsy <- table(m_all_unique$Cohort[m_all_unique$study_id %in% biopsy_study_ids])
perc_biopsy <- n_biopsy / n_cohort * 100

stats_table <- t(cbind(names(n_cohort), n_cohort, ages, ages_sd, bhi, bhi_sd, n_female, perc_female, n_biopsy ,perc_biopsy))
write.csv(stats_table, "stats_table_cohort.csv", row.names = T)

#------------------------------------------------------------




