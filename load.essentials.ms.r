#Set your working directory to be a new directory within this main one
library('vegan')
library('RColorBrewer')
library('ggplot2')
library('ggpubr')
library('reshape2')
library('plyr')
library('ape')
library('grid')
library('gridExtra')
library('cowplot')
library('ROCR')
library('flux')
library('randomForest')
library('robCompositions')
library('polycor')
library('qvalue')
library('limma')
library('dplyr')


setwd("~/Dropbox/Mayo_postdoc/Tonya20190701_ibs_project-master/code")

source("../code/output.dirs.ms.r")
source('../code/stats.ms.r')
source('../code/util.ms.r')

#Load file paths for inputs
DATADIR <- '../data/IBS_Mayo_secondrun/'
metadata <- paste(DATADIR, "Final_Cleaned_Condensed_Metadata.csv", sep='')
metadatabiopsy <- paste(DATADIR, "mucosal_bacteria_condensed_metadata_file_v2.csv", sep="")
taxfp <- paste(DATADIR,'/shogun_output/id_update/taxatable.burst.capitalist.txt',sep='')
taxbiopyfp <- paste(DATADIR, 'shogun_output_biopsy/taxatable.burst.capitalist.txt', sep="")
taxALLfp <- paste(DATADIR, 'stool_biopsy_otus.txt', sep="")
modulefp <- paste(DATADIR, 'shogun_output/id_update/taxatable.strain.kegg.modules.txt', sep='')
module_mapfp <- paste(DATADIR, 'Kegg_ID_Map.txt', sep='')
keggfp <- paste(DATADIR, 'shogun_output/id_update/taxatable.strain.kegg.txt', sep='')
keggmapfp <- paste(DATADIR, "ko-enzyme-annotations.txt", sep='')
pathwayfp <- paste(DATADIR, 'shogun_output/id_update/taxatable.strain.kegg.pathways.txt', sep='')

#Load metadata (this was generated with metadata_prep.r)
m <- read.delim(metadata, 
                   sep=',', 
                   head=T, 
                   comment="",
                   row=1)
m$SampleID <- rownames(m)

m_bio <- read.delim(metadatabiopsy, 
                sep=',', 
                head=T, 
                comment="",
                quote="",
                row=1)
m_bio$SampleID <- rownames(m_bio)
m_bio$Timepoint <- gsub("nd", "", m_bio$Timepoint)
m_bio$Timepoint <- gsub("st", "", m_bio$Timepoint)

#Read in the OTU table, reformat the names of the samples, remove duplicates
x <- t(read.delim(taxfp, row=1, check.names=F, as.is =T)) # 485 samples 2343 OTUS
taxa_names <- colnames(x) #store this because it replaces ";" with "." later

x_bio <- t(read.delim(taxbiopyfp, row=1, check.names = F, as.is=T)) #72 samples 610 OTUS
taxa_bio <- colnames(x_bio)

#Find samples that are still not accounted for:
keeps <- intersect(rownames(x), rownames(m)) #481 samples overlap between the OTU table and metadata
not_in_map <- rownames(x)[which(!rownames(x) %in% rownames(m))] 
cat("These samples aren't in the map:") #negative control samples
not_in_map

not_in_otu <- rownames(m)[which(!rownames(m) %in% rownames(x))] 
cat("These samples aren't in the otu table:") #9 samples did not make it for whatever reason
not_in_otu 

not_in_biospy <- rownames(m_bio)[which(!rownames(m_bio) %in% rownames(x_bio))]
patients_no_biopsy <- m_bio$ID_on_tube[which(!rownames(m_bio) %in% rownames(x_bio))]
cat("these many patients don't have biopsy sequences:") #no one
unique(patients_no_biopsy)
cat("These samples aren't in the biopsy otu table:")
not_in_biospy
keeps <- intersect(rownames(x_bio), rownames(m_bio))
x_bio <- x_bio[keeps,]
m_bio <- m_bio[keeps,]

#remove samples without info in the mapping file (previously called "family" samples)
m <- m[!is.na(m$study_id),]

#Keep only samples with 10000 counts
#original = 485 samples, 2343 OTUs 
x <- x[rowSums(x) > 10000,] #474 samples pass, 3 do not
keeps <- intersect(rownames(x), rownames(m))
m <- m[keeps,]
x <- x[keeps,] #474 2343

#Add aligned sequence counts to map
m$counts <- rowSums(x)
colnames(x) <- taxa_names #replace proper taxa names

#same for biopsy
m_bio$counts <- rowSums(x_bio) 

#copied archaea to sep table, n of archaea = 12
archaea <- x[,grep("Archaea", colnames(x))]
m$archaea <- rowSums(archaea)

#same for biopsy,  n of archaea = 2
archea_bio <- x_bio[,grep("Archaea", colnames(x_bio))]
m_bio$archea <- rowSums(archea_bio)

#copied viruses/phage to sep table = 278
viruses <- x[,grep("Viruses", colnames(x))]
m$viruses <- rowSums(viruses)

#same for biopsy, n of viruses  = 0
viruses_bio <- x_bio[,grep("Viruses", colnames(x_bio))]
m_bio$viruses <- rowSums(viruses_bio)

#store only bacteria from main table in xb
xb <- x[,grep("Bacteria", colnames(x))] #these are 2053 OTUs
m$bacteria <- rowSums(xb) 

#transpose datafiles
x <- t(x)
x_bio <- t(x_bio)

#-------------------------------------------------------
#Stool
#Drop viruses

x <- x[-grep("Viruses", rownames(x)),]

#-------------------------------------------------------
#no viruses in biopsies

#-------------------------------------------------------
#Stool
#for stool table split rownames on delim ";", make a dataframe of these
split <- strsplit(rownames(x),";")
full_taxa_names <- rownames(x)
species_names <- c()
for(i in 1:length(split)){
  if(length(split[[i]]) > 6){
    species_names[i] <- split[[i]][7] #If it has species, keep that
  } else {
    species_names[i] <- paste(split[[i]][1:(length(split[[i]]))],collapse="_") #Or collapse down to most specific
  }
}
dim(x) #one more "sample" / column, "Group.1"

phylum_list <- lapply(split, function(x) {x[grep("p__", x)]})
order_list <- lapply(split, function(x) {x[grep("o__", x)]})
family_list <- lapply(split, function(x) {x[grep("f__", x)]})
genus_list <- lapply(split, function(x) {x[grep("g__", x)]})

tax_df <- as.data.frame(do.call(cbind, list(unlist(phylum_list), unlist(order_list), unlist(family_list), unlist(genus_list))))
tax_df <- cbind(tax_df, species_names)
names(tax_df) <- c("phylum", "order", "family", "genus", "species")

#-----------------------------
#Stool
#aggregating at different taxonomic levels and subsetting. 
#Later write to excel file with separate sheets
#make same list with normalized counts

#aggregate at different taxonomic levels
x_raw_tax_list <- list()
for (i in 1:length(colnames(tax_df))) {
  x_raw_tax_list[[i]] <- aggregate(x, by=list(tax_df[,i]),sum)
  temp_names <- x_raw_tax_list[[i]]$Group.1
  x_raw_tax_list[[i]] <- sapply(x_raw_tax_list[[i]][,-1], function(y) {as.numeric(as.character(y))}) #remove first col
  rownames(x_raw_tax_list[[i]]) <- temp_names
}
names(x_raw_tax_list) <- names(tax_df)
x_RA_tax_list <- lapply(x_raw_tax_list, function(y) {sweep(y, 2, colSums(y), '/')})



#-------------------------------------------------------
#Biopsies
#same for for biopsy table split rownames on delim ";", make a dataframe of these
split <- strsplit(rownames(x_bio),";")
species_names <- c()
for(i in 1:length(split)){
  if(length(split[[i]]) > 6){
    species_names[i] <- split[[i]][7] #If it has species, keep that
  } else {
    species_names[i] <- paste(split[[i]][1:(length(split[[i]]))],collapse="_") #Or collapse down to most specific
  }
}

phylum_list <- lapply(split, function(x) {x[grep("p__", x)]})
order_list <- lapply(split, function(x) {x[grep("o__", x)]})
family_list <- lapply(split, function(x) {x[grep("f__", x)]})
genus_list <- lapply(split, function(x) {x[grep("g__", x)]})

tax_df_biop <- as.data.frame(do.call(cbind, list(unlist(phylum_list), unlist(order_list), unlist(family_list), unlist(genus_list))))
tax_df_biop <- cbind(tax_df_biop, species_names)
names(tax_df_biop) <- c("phylum", "order", "family", "genus", "species")


#-----------------------------
#Biopsies
#aggregating at different taxonomic levels and subsetting. 
#Later write to excel file with separate sheets
#make same list with normalized counts

#preparing metadata for updating sample IDs
#combine into unique ID
m_bio$unique_id <- paste(m_bio$study_id, m_bio$Timepoint, sep = "_")

#aggregate at different taxonomic levels
xbio_raw_tax_list <- list()
for (i in 1:length(colnames(tax_df_biop))) {
  xbio_raw_tax_list[[i]] <- aggregate(x_bio, by=list(tax_df_biop[,i]),sum)
  colnames(xbio_raw_tax_list[[i]])[2:ncol(xbio_raw_tax_list[[i]])] <- m_bio$unique_id 
  temp_names <- xbio_raw_tax_list[[i]]$Group.1
  xbio_raw_tax_list[[i]] <- sapply(xbio_raw_tax_list[[i]][,-1], function(y) {as.numeric(as.character(y))}) #remove first row
  rownames(xbio_raw_tax_list[[i]]) <- temp_names
}
names(xbio_raw_tax_list) <- names(tax_df_biop)
xbio_RA_tax_list <- lapply(xbio_raw_tax_list, function(y) {sweep(y, 2, colSums(y), '/')})


#------------------------------
#Stool
#get averages per subject, without flares and including flares to see if this makes a difference 
#do selecting most relevant species on averaged abundances per subject

#-------------------------------------------------------
#subsetting low abundant species first collapse by subject
#identifying them from the counts table only, remove in both.
#without flares for later analyses, get column indices of flare samples
m$ID_on_tube <- as.integer(m$ID_on_tube)
flare_cols <- which(m$Timepoint == "Flare")

stool_RA_collapsed_tax_list_wF <- list()
stool_RA_collapsed_tax_list_woF <- list()
stool_counts_collapsed_tax_list_wF <- list()
stool_counts_collapsed_tax_list_woF <- list()
rare_otus_list_wF <- list()
rare_otus_list_woF <- list()

for (i in 1:length(x_RA_tax_list)) {
  temp_data <- t(x_RA_tax_list[[i]])
  temp_data_mean_wF <- apply(temp_data,2,function(xx) sapply(split(xx,m$ID_on_tube),mean))
  rownames(temp_data_mean_wF) <- sprintf('Subject_%03d',sapply(split(m$ID_on_tube,m$ID_on_tube),'[',1))
  rare_otus_list_wF[[i]] <- which(colMeans(temp_data_mean_wF > 0) < .10)
  stool_RA_collapsed_tax_list_wF[[i]] <- temp_data_mean_wF
  
  temp_data_woF <- temp_data[-flare_cols,]
  m_woF <- m[-flare_cols,]
  temp_data_mean_woF <- apply(temp_data_woF,2,function(xx) sapply(split(xx,m_woF$ID_on_tube),mean))
  rownames(temp_data_mean_woF) <- sprintf('Subject_%03d',sapply(split(m_woF$ID_on_tube,m_woF$ID_on_tube),'[',1))
  rare_otus_list_woF[[i]] <- which(colMeans(temp_data_mean_woF > 0) < .10)
  stool_RA_collapsed_tax_list_woF[[i]] <- temp_data_mean_woF
  
  temp_data <- t(x_raw_tax_list[[i]])
  temp_data_mean_wF <- apply(temp_data,2,function(xx) sapply(split(xx,m$ID_on_tube),mean))
  rownames(temp_data_mean_wF) <- sprintf('Subject_%03d',sapply(split(m$ID_on_tube,m$ID_on_tube),'[',1))
  stool_counts_collapsed_tax_list_wF[[i]] <- temp_data_mean_wF
  
  temp_data_woF <- temp_data[-flare_cols,]
  m_woF <- m[-flare_cols,]
  temp_data_mean_woF <- apply(temp_data_woF,2,function(xx) sapply(split(xx,m_woF$ID_on_tube),mean))
  rownames(temp_data_mean_woF) <- sprintf('Subject_%03d',sapply(split(m_woF$ID_on_tube,m_woF$ID_on_tube),'[',1))
  stool_counts_collapsed_tax_list_woF[[i]] <- temp_data_mean_woF
}


#averaging the metadata
m_collapsed_wF <- m[sapply(split(1:nrow(m),m$ID_on_tube),'[',1),,drop=TRUE]
rownames(m_collapsed_wF) <- sprintf('Subject_%03d',sapply(split(m$ID_on_tube,m$ID_on_tube),'[',1))

m_collapsed_woF <- m_woF[sapply(split(1:nrow(m_woF),m_woF$ID_on_tube),'[',1),,drop=TRUE]
rownames(m_collapsed_woF) <- sprintf('Subject_%03d',sapply(split(m_woF$ID_on_tube,m_woF$ID_on_tube),'[',1))

#-------------------------------------------------------
#setdiff_sym_specifics function
setdiff_sym_specifics <- function(x, y) {
  cat("only in x:")
  print(setdiff(x,y))
  cat("only in y:")
  print(setdiff(y,x))
}

#without including flares is a bit more conservative with losing species. Nothing missing when flares are included
#setdiff_sym_specifics(rare_otus_list_wF[[4]], rare_otus_list_woF[[4]]) 


#-------------------------------------------------------
#preparing metadata for updating sample IDs

#update names and save as R objects
m$Timepoint_full <-  m$Timepoint
m$Timepoint_full <- as.character(m$Timepoint_full)
flare_rows <- grep("Flare", m$SampleID)
m$Timepoint_full[flare_rows] <- m$Flare_timepoint.y[flare_rows]
#combine into unique 
m$unique_id <- paste(m$study_id, m$Timepoint_full, sep = "_")

#-------------------------------------------------------
#Stool
#Drop rare species and add names column
#use rare_otus_list_wF for subsetting both files x_raw_tax_list and x_RA_tax_list

x_raw_tax_list_sub <- x_raw_tax_list
x_RA_tax_list_sub <- x_RA_tax_list
x_RA_collapsed_tax_list_wF_sub <- lapply(stool_RA_collapsed_tax_list_wF, t) #was already transposed
x_RA_collapsed_tax_list_woF_sub <- lapply(stool_RA_collapsed_tax_list_woF, t)#was already transposed
x_raw_collapsed_tax_list_woF_sub <- lapply(stool_counts_collapsed_tax_list_woF, t)#was already transposed

for (i in 1:length(rare_otus_list_wF)) {
  to_rm <- rare_otus_list_wF[[i]]
  x_raw_tax_list_sub[[i]] <- as.data.frame(x_raw_tax_list[[i]][-to_rm, ])
  x_RA_tax_list_sub[[i]] <- as.data.frame(x_RA_tax_list[[i]][-to_rm, ])
  x_RA_collapsed_tax_list_wF_sub[[i]] <- as.data.frame(t(stool_RA_collapsed_tax_list_wF[[i]])[-to_rm,])
  x_RA_collapsed_tax_list_woF_sub[[i]] <- as.data.frame(t(stool_RA_collapsed_tax_list_woF[[i]])[-to_rm,])
  x_raw_collapsed_tax_list_woF_sub[[i]] <- as.data.frame(t(stool_counts_collapsed_tax_list_woF[[i]])[-to_rm,])
  #updating colnames
  colnames(x_raw_tax_list_sub[[i]]) <- m$unique_id 
  colnames(x_RA_tax_list_sub[[i]]) <- m$unique_id 
  colnames(x_RA_collapsed_tax_list_wF_sub[[i]]) <- m_collapsed_wF$study_id
  colnames(x_RA_collapsed_tax_list_woF_sub[[i]]) <- m_collapsed_woF$study_id
  colnames(x_raw_collapsed_tax_list_woF_sub[[i]]) <- m_collapsed_woF$study_id
  #adding taxa name column
  x_raw_tax_list_sub[[i]]$taxa <- rownames(x_raw_tax_list[[i]][-to_rm, ])
  x_RA_tax_list_sub[[i]]$taxa <- rownames(x_RA_tax_list[[i]][-to_rm, ])
  x_RA_collapsed_tax_list_wF_sub[[i]]$taxa <- rownames(x_RA_tax_list[[i]][-to_rm, ])
  x_RA_collapsed_tax_list_woF_sub[[i]]$taxa <- rownames(x_RA_tax_list[[i]][-to_rm, ])
  x_raw_collapsed_tax_list_woF_sub[[i]]$taxa <- rownames(x_RA_tax_list[[i]][-to_rm, ])
  #adding rownames for statistics input
  rownames(x_raw_tax_list_sub[[i]]) <- x_raw_tax_list_sub[[i]]$taxa  
  rownames(x_RA_tax_list_sub[[i]]) <- x_RA_tax_list_sub[[i]]$taxa
  rownames(x_RA_collapsed_tax_list_wF_sub[[i]]) <- x_RA_collapsed_tax_list_wF_sub[[i]]$taxa 
  rownames(x_RA_collapsed_tax_list_woF_sub[[i]]) <- x_RA_collapsed_tax_list_woF_sub[[i]]$taxa
  rownames(x_raw_collapsed_tax_list_woF_sub[[i]]) <- x_raw_collapsed_tax_list_woF_sub[[i]]$taxa
  #moving taxa name to the front
  x_raw_tax_list_sub[[i]] <- x_raw_tax_list_sub[[i]][,c(ncol(x_raw_tax_list_sub[[i]]), (1:ncol(x_raw_tax_list_sub[[i]])-1)),]
  x_RA_tax_list_sub[[i]] <- x_RA_tax_list_sub[[i]][,c(ncol(x_RA_tax_list_sub[[i]]), (1:ncol(x_RA_tax_list_sub[[i]])-1)),]
  x_RA_collapsed_tax_list_wF_sub[[i]] <- x_RA_collapsed_tax_list_wF_sub[[i]][,c(ncol(x_RA_collapsed_tax_list_wF_sub[[i]]), 
                                                                                (1:ncol(x_RA_collapsed_tax_list_wF_sub[[i]])-1)),]
  x_RA_collapsed_tax_list_woF_sub[[i]] <- x_RA_collapsed_tax_list_woF_sub[[i]][,c(ncol(x_RA_collapsed_tax_list_woF_sub[[i]]), 
                                                                                  (1:ncol(x_RA_collapsed_tax_list_woF_sub[[i]])-1)),]
  x_raw_collapsed_tax_list_woF_sub[[i]] <- x_raw_collapsed_tax_list_woF_sub[[i]][,c(ncol(x_raw_collapsed_tax_list_woF_sub[[i]]), 
                                                                                  (1:ncol(x_raw_collapsed_tax_list_woF_sub[[i]])-1)),]
}

#identical dimensions for the subsetted data
names(x_raw_tax_list_sub) <- names(x_raw_tax_list)
names(x_RA_tax_list_sub) <- names(x_raw_tax_list)
names(x_RA_collapsed_tax_list_wF_sub) <- names(x_raw_tax_list)
names(x_RA_collapsed_tax_list_woF_sub) <- names(x_raw_tax_list)
names(x_raw_collapsed_tax_list_woF_sub) <- names(x_raw_tax_list)

lapply(x_raw_collapsed_tax_list_woF_sub, dim)
#$phylum [1] 17 76
#$order [1] 66 76
#$family [1] 133  76
#$genus [1] 373  76
#$species [1] 1009   76

#-------------------------------------------------------
#get full names back for maaslin input
full_taxa_names_sub <- sapply(x_RA_tax_list_sub$species$taxa, function(x) {full_taxa_names[grep(gsub("s__", "", x), full_taxa_names, fixed=T)][1]})
full_taxa_names_sub_vec <- unlist(full_taxa_names_sub)
temp_data <- x_RA_tax_list_sub$species[,2:ncol(x_RA_tax_list_sub$species)]
rownames(temp_data) <- full_taxa_names_sub_vec

colnames(temp_data) <- m$SampleID

write.table(temp_data, file="IBS_microbiome_phylogenetic_levels_RA_species.txt", sep="\t")

#-------------------------------------------------------
#Stool
#write excel files. x_RA_tax_list_sub, x_raw_tax_list_sub, and x_RA_collapsed_tax_list_woF_sub

require(openxlsx)

save(x_RA_tax_list_sub, file="IBS_microbiome_phylogenetic_levels_RA.Rdata")
#load("IBS_microbiome_phylogenetic_levels_RA.Rdata")
#names(x_RA_tax_list_sub)
filename <- "IBS_microbiome_phylogenetic_levels_RA.xlsx"
## setup a workbook with appropriate number of worksheets
wb <- createWorkbook()
for (i in 1:length(x_RA_tax_list_sub)) {
  addWorksheet(wb = wb, sheetName = names(x_RA_tax_list_sub)[i], gridLines = T)
  writeData(wb = wb, sheet = i, x = x_RA_tax_list_sub[[i]])
}
saveWorkbook(wb, filename, overwrite = TRUE)


save(x_raw_tax_list_sub, file="IBS_microbiome_phylogenetic_levels_counts.Rdata")
#load("IBS_microbiome_phylogenetic_levels_counts.Rdata")
#names(x_raw_tax_list_sub)
filename <- "IBS_microbiome_phylogenetic_levels_counts.xlsx"
wb <- createWorkbook()
for (i in 1:length(x_raw_tax_list_sub)) {
  addWorksheet(wb = wb, sheetName = names(x_raw_tax_list_sub)[i], gridLines = T)
  writeData(wb = wb, sheet = i, x = x_raw_tax_list_sub[[i]])
}
saveWorkbook(wb, filename, overwrite = TRUE)


save(x_RA_collapsed_tax_list_woF_sub, file="IBS_microbiome_phylogenetic_levels_RA_collapsed.Rdata")
#load("IBS_microbiome_phylogenetic_levels_RA_collapsed.Rdata")
#names(x_RA_collapsed_tax_list_woF_sub)
filename <- "IBS_microbiome_phylogenetic_levels_RA_collapsed.xlsx"
wb <- createWorkbook()
for (i in 1:length(x_RA_collapsed_tax_list_woF_sub)) {
  addWorksheet(wb = wb, sheetName = names(x_RA_collapsed_tax_list_woF_sub)[i], gridLines = T)
  writeData(wb = wb, sheet = i, x = x_RA_collapsed_tax_list_woF_sub[[i]])
}
saveWorkbook(wb, filename, overwrite = TRUE)



save(x_raw_collapsed_tax_list_woF_sub, file="IBS_microbiome_phylogenetic_levels_counts_collapsed.Rdata")
#load("IBS_microbiome_phylogenetic_levels_counts_collapsed.Rdata")
#names(x_raw_collapsed_tax_list_woF_sub)
filename <- "IBS_microbiome_phylogenetic_levels_counts_collapsed.xlsx"
wb <- createWorkbook()
for (i in 1:length(x_raw_collapsed_tax_list_woF_sub)) {
  addWorksheet(wb = wb, sheetName = names(x_raw_collapsed_tax_list_woF_sub)[i], gridLines = T)
  writeData(wb = wb, sheet = i, x = x_raw_collapsed_tax_list_woF_sub[[i]])
}
saveWorkbook(wb, filename, overwrite = TRUE)


#-------------------------------------------------------
#Biopsy
#remove rare species, present in less than 5% of samples.

xbio_raw_tax_list_sub <- xbio_raw_tax_list
xbio_RA_tax_list_sub <- xbio_RA_tax_list
for (i in 1:length(xbio_RA_tax_list)) {
  temp_data <- t(xbio_RA_tax_list[[i]])
  to_rm <- which(colMeans(temp_data > 0) < .05)
  xbio_raw_tax_list_sub[[i]] <- as.data.frame(xbio_raw_tax_list[[i]][-to_rm, ])
  xbio_RA_tax_list_sub[[i]] <- as.data.frame(xbio_RA_tax_list[[i]][-to_rm, ])
  xbio_raw_tax_list_sub[[i]]$taxa <- rownames(xbio_raw_tax_list[[i]][-to_rm, ])
  xbio_RA_tax_list_sub[[i]]$taxa <- rownames(xbio_RA_tax_list[[i]][-to_rm, ])
  xbio_raw_tax_list_sub[[i]] <- xbio_raw_tax_list_sub[[i]][,c(ncol(xbio_raw_tax_list_sub[[i]]), (1:ncol(xbio_raw_tax_list_sub[[i]])-1)),]
  xbio_RA_tax_list_sub[[i]] <- xbio_RA_tax_list_sub[[i]][,c(ncol(xbio_RA_tax_list_sub[[i]]), (1:ncol(xbio_RA_tax_list_sub[[i]])-1)),]
}

#lapply(xbio_raw_tax_list_sub, dim)

#$phylum [1]  9 73
#$order [1] 35 73
#$family [1] 67 73
#$genus [1] 143  73
#$species [1] 294  73


#-------------------------------------------------------
#Biopsy
#write excel files.

require(openxlsx)

save(xbio_RA_tax_list_sub, file="IBS_biopsy_16S_phylogenetic_levels_RA.Rdata")
#load("IBS_biopsy_16S_phylogenetic_levels_RA.Rdata")
#names(xbio_RA_tax_list_sub)
filename <- "IBS_biopsy_16S_phylogenetic_levels_RA.xlsx"
## setup a workbook with appropriate number of worksheets
wb <- createWorkbook()
for (i in 1:length(xbio_RA_tax_list_sub)) {
  addWorksheet(wb = wb, sheetName = names(xbio_RA_tax_list_sub)[i], gridLines = T)
  writeData(wb = wb, sheet = i, x = xbio_RA_tax_list_sub[[i]])
}
saveWorkbook(wb, filename, overwrite = TRUE)


save(xbio_raw_tax_list_sub, file="IBS_biopsy_16S_phylogenetic_levels_counts.Rdata")
#load("IBS_biopsy_16S_phylogenetic_levels_counts.Rdata")
#names(xbio_raw_tax_list_sub)
filename <- "IBS_biopsy_16S_phylogenetic_levels_counts.xlsx"
wb <- createWorkbook()
for (i in 1:length(xbio_raw_tax_list_sub)) {
  addWorksheet(wb = wb, sheetName = names(xbio_raw_tax_list_sub)[i], gridLines = T)
  writeData(wb = wb, sheet = i, x = xbio_raw_tax_list_sub[[i]])
}
saveWorkbook(wb, filename, overwrite = TRUE)

#-------------------------------------------------------
#-------------------------------------------------------
# gather differentiation indicies and set up colors

mc <- m_collapsed_woF

ixc.hc <- mc$Cohort == "H"
ix.hc <- m$Cohort == "H" & is.na(m$Flare)
ixc.ibsc <- mc$Cohort == "C"
ix.ibsc <- m$Cohort == "C" & is.na(m$Flare)
ixc.ibsd <- mc$Cohort == "D"
ix.ibsd <- m$Cohort == "D" & is.na(m$Flare)
ixc.ibs <- mc$Cohort != "H"
ix.ibs <- m$Cohort != "H" & is.na(m$Flare)

ixcb.hc <- m_bio$Cohort == "H"
ixcb.ibs <- m_bio$Cohort != "H"
ixcb.ibsd <- m_bio$Cohort == "D"
ixcb.ibsc <- m_bio$Cohort == "C"


#-------------------------------------------------------
#update names from medadata
mc$IBS <- as.character(mc$Cohort)
mc[mc$IBS == "H", "IBS"] <- "Healthy"
mc[mc$IBS == "C" | mc$IBS == "D", "IBS"] <- "IBS"

m$IBS <- as.character(m$Cohort)
m[m$IBS == "H" & !is.na(m$IBS), "IBS"] <- "Healthy"
m[!m$IBS == "Healthy" & !is.na(m$IBS), "IBS"] <- "IBS"

mc$IBS <- factor(mc$IBS)
m$IBS <- factor(m$IBS)

m$Cohort <- as.character(m$Cohort)

#-------------------------------------------------------
#update files for downstream processing, colnames should be taxa, rownames should be sampleIDs

x <- t(x_RA_tax_list_sub$species[,-1])
colnames(x) <- x_RA_tax_list_sub$species$taxa
rownames(x) <- rownames(m)

xc <- t(x_RA_collapsed_tax_list_woF_sub$species[,-1])
colnames(xc) <- x_RA_collapsed_tax_list_woF_sub$species$taxa
rownames(xc) <- rownames(mc)

xc_wF <- t(x_RA_collapsed_tax_list_wF_sub$species[,-1])
colnames(xc_wF) <- x_RA_collapsed_tax_list_wF_sub$species$taxa
rownames(xc_wF) <- rownames(xc_wF)

x_bio <- t(xbio_RA_tax_list_sub$species[,-1])
colnames(x_bio) <- xbio_RA_tax_list_sub$species$taxa
rownames(x_bio) <- rownames(m_bio)


#-------------------------------------------------------
#-------------------------------------------------------
# Load Modules
modules <- t(read.table(modulefp, header=T, sep='\t', row=1, comment='', check.names = F, as.is=T))
modules <- modules[rownames(m),]
modules <- modules[,colSums(modules)>0]
rare.ix <- colMeans(modules > 0) < .25 #25% modules
modules <- modules[,!rare.ix]

coef.variation <- function(xx) {
  sqrt(var(xx))/mean(xx)
}

modules <- data.frame(modules)
colnames(modules) <- gsub("\\.", "",colnames(modules))
co_varies <- sapply(modules[,colnames(modules)],coef.variation)
modules <- modules[,colnames(modules)[order(co_varies, decreasing = T)]] #sort by covariance

#Collapse by subject
modules2 <- modules[!m$Timepoint == "Flare",] #Leave out Flares before averaging
m2 <- m[!m$Timepoint == "Flare",]

#-------------------------------------------------------
#Load module map metadata file
module_names <- read.table(module_mapfp, sep="\t", header=T, as.is=T)
module_names$module <- sapply(strsplit(as.character(module_names$KEGG_Label),'\ '), "[", 1)
module_names$BugBase_ID <- gsub(".*?_(.+)", "\\1", module_names$BugBase_ID)
rownames(module_names) <- module_names$module
module_names <- module_names[colnames(modules),]
colnames(modules) <- module_names$BugBase_ID
colnames(modules2) <- module_names$BugBase_ID

#collapsed without flares:
modulesc <- apply(modules2,2,function(xx) sapply(split(xx,m2$ID_on_tube),mean))
modulesc <- modulesc[,colSums(modulesc) > 0]
rownames(modulesc) <- sprintf('Subject_%03d',sapply(split(m$ID_on_tube,m$ID_on_tube),'[',1))
#collapsed with flares:
modulesc_flares <- apply(modules,2,function(xx) sapply(split(xx,m$ID_on_tube),mean))
rownames(modulesc_flares) <- sprintf('Subject_%03d',sapply(split(m$ID_on_tube,m$ID_on_tube),'[',1))

# Load KEGG
kegg <- t(read.table(keggfp, header=T, sep='\t', row=1, comment='', check.names=F, as.is=T)) #485 samples by 7332 kegg
kegg <- kegg[rownames(m),]
kegg <- kegg[,colSums(kegg)>1]
rare.ix <- colMeans(kegg > 0) < .25
kegg <- kegg[,!rare.ix] #now 474 samples 5453 modules

k_map <- read.table(keggmapfp, sep='\t', comment='') #2326 keggs, 5 cols
keeps <- intersect(colnames(kegg), k_map$V1)
k_map <- k_map[k_map$V1 %in% keeps,] #now 2326

#Collapse by subject
kegg2 <- kegg[!m$Timepoint == "Flare",] #Leave out Flares before averaging
#keggc is collapsed without flares
keggc <- apply(kegg2,2,function(xx) sapply(split(xx,m2$ID_on_tube),mean))
keggc <- keggc[,colSums(keggc) > 0]
rownames(keggc) <- sprintf('Subject_%03d',sapply(split(m$ID_on_tube,m$ID_on_tube),'[',1))
#keggf_flares is collapsed with flares
keggc_flares <- apply(kegg,2,function(xx) sapply(split(xx,m$ID_on_tube),mean))
rownames(keggc_flares) <- sprintf('Subject_%03d',sapply(split(m$ID_on_tube,m$ID_on_tube),'[',1))

#write kegg and keggc files to .txt
write.table(keggc, file='keggc_collapsed_KOterms.tsv', quote=FALSE, sep='\t')
write.table(kegg, file='kegg_all_KOterms.tsv', quote=FALSE, sep='\t')


#-------------------------------------------------------
#make metadata all plot to use as input for timeline plot

#Load the full OTU table biopsies and stool combined (combined in qiime and mod names, etc)
x_all <- t(read.delim(taxALLfp, row=1, skip=1, check.names=F, as.is =T)) #546 samples by 1545 OTUs
taxa_names <- colnames(x_all) #store this because it replaces ";" with "." later
m$sType <- m$Flare
m$sType <- as.character(m$sType)
m["sType"][is.na(m["sType"])] <- "Stool"
m_bio$sType <- rep("Biopsy", nrow(m_bio))

map_cols <- intersect(colnames(m), colnames(m_bio))
m_all <- data.frame(rbind(m[,map_cols], m_bio[,map_cols]))

keeps <- intersect(rownames(x_all), rownames(m_all))
x_all <- x_all[keeps,] #546 samples
m_all <- m_all[keeps,]

x_all.raw <- x_all #store raw
x_all <- sweep(x_all, 1, rowSums(x_all), '/') #store RA

# drop rare bugs (show up in less than 5 subjects)
# Goes to 1095 OTUs
rare.ix <- colSums(x_all > 0) < 5
x_all.raw <- x_all.raw[,!rare.ix]
x_all <- x_all[,!rare.ix]

map_cols <- intersect(colnames(m), colnames(m_bio))
m_all <- data.frame(rbind(m[,map_cols], m_bio[,map_cols]))

keeps <- intersect(rownames(x_all), rownames(m_all))
x_all <- x_all[keeps,] #527 samples, 1483
m_all <- m_all[keeps,]
#table(m_all$sType)
#Biopsy  Flare  Stool 
#53     12    462 


#-------------------------------------------------------
#how many timelines are complete?
complete_timeline <- c(0, 0, 0)
names(complete_timeline) <- c("H", "C", "D")
for(i in 1:length(unique(m$study_id))){
  working <- unique(m$study_id)[i]
  if(! is.na(working)){
    if(length(which(m$study_id == working)) >= 7){
      cohort <- as.character(m[which(m$study_id == working),"Cohort"])[1]
      complete_timeline[cohort] <- c(complete_timeline[cohort] + 1)
    }
  }
}
print("Complete Timelines:")
print(complete_timeline) #only 7 samples was considered complete before, now 7 or more because of flares

#H  C  D 
#exactly 7:   17 13 17 
#7 or more:   17 19 22

#How many have paired biopsies?
complete_timeline <- c(0, 0, 0)
names(complete_timeline) <- c("H", "C", "D")
for(i in 1:length(unique(m_bio$study_id))){
  working <- unique(m_bio$study_id)[i]
  if(! is.na(working)){
    if(length(which(m_bio$study_id == working)) == 2){
      cohort <- as.character(m_bio[which(m_bio$study_id == working),"Cohort"])[1]
      complete_timeline[cohort] <- c(complete_timeline[cohort] + 1)
    }
  }
}
print("2 biopsies:")
print(complete_timeline)
# H  C  D 
#people with 2 biopsies: 9 10 11 


#what are the average number of samples each person has?
average_timeline <- list(NA, NA, NA)
names(average_timeline) <- c("H", "C", "D")
for(i in 1:length(unique(m$study_id))){
  working <- unique(m$study_id)[i]
  if(! is.na(working)){
    cohort <- as.character(m[which(m$study_id == working),"Cohort"])[1]
    average_timeline[[cohort]]<- c(average_timeline[[cohort]], length(which(m$study_id == working)))
  }
}
for(i in 1:length(average_timeline)){
  average_timeline[[i]] <- mean(average_timeline[[i]], na.rm = T)
}
print("Average Length of Timelines:")
print(average_timeline)


#Number of subjects with =< 4 samples
names(average_timeline) <- c("H", "C", "D")
for(i in 1:length(unique(m$study_id))){
  working <- unique(m$study_id)[i]
  if(! is.na(working)){
    cohort <- as.character(m[which(m$study_id == working),"Cohort"])[1]
    if(length(which(m$study_id == working)) > 3){
      average_timeline[[cohort]]<- average_timeline[[cohort]] + 1
    }
  }
}
print("Number of subjects with >= 4 samples:")
print(average_timeline)
#$H [1] 29.25
#$C [1] 26.72727
#$D [1] 29.06897




