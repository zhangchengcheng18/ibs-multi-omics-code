---
title: "IBS biopsy metabolomics"
author: "Ruben Mars"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document: default
  pdf_document: default
---


#### This is an R Markdown document.

## Goal
Analyzing metabolomics data from biopsies


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, 
                      fig.width = 8, #this also affects the size of the labels
                      fig.asp = 0.618)

require(RColorBrewer)
require(readxl)
require(openxlsx)
library(psych)
library(dplyr)

setwd("~/Dropbox/Mayo_postdoc/Gitlab_mayo/general_functions/")
source("replace_na_with_median.R")
source("replace_outliers_with_median.R")
source("anova_Tukey.R")
source("lmer_contrasts_FC.R")
source("linear_models_adjusting_pvals.R")
source("time_course_filtering.R")
source("PCA_and_plot.R")

source("initiate_IBS_plotting.R")
source("make_boxplot_figure.R")

```


```{r metabolomics SCFA biopsies, message=FALSE, warning=FALSE}

setwd("/Users/m210320/Dropbox/IBS/Metabonomics/data/Biopsy_SCFA/")
sample_name <- "biopsies_summary_noFirstRow.csv"
biop_mbx_df <- as.data.frame(read.csv(sample_name, header=T, sep=","), stringsAsFactors=F)

biop_mbx_meta <- biop_mbx_df[,-c(5:ncol(biop_mbx_df))]
biop_mbx_meta$Cohort_full <- as.character(biop_mbx_meta$Cohort)

biop_mbx_data <- t(biop_mbx_df[,5:ncol(biop_mbx_df)])

metabolite_names <- tolower(rownames(biop_mbx_data))
metabolite_names <- gsub("x2.", "", metabolite_names)

biop_mbx_meta$Subject_ID <- biop_mbx_meta$Patient.ID
biop_mbx_meta$Subject_ID <- gsub("-2", "", biop_mbx_meta$Subject_ID)
biop_mbx_meta$unique_id <- paste(biop_mbx_meta$Subject_ID, biop_mbx_meta$Visit.number, sep = "_")

colnames(biop_mbx_data) <- biop_mbx_meta$unique_id 

#values below 0 replaced by 1's
biop_mbx_data[which(biop_mbx_data < 0)] <- 1

save(biop_mbx_data, file="IBS_metabolomics_biopsy_SCFA_all.Rdata")


```


```{r statistics on the data, message=FALSE, warning=FALSE}

#log10 transformation improves the statistics. Acetate only significant
lmer_res_list <- lmer_contrasts_FC(log10(biop_mbx_data), biop_mbx_meta$Cohort_full, biop_mbx_meta$Patient.ID, row_names=metabolite_names)
sign_metabolites <- lapply(lmer_res_list$contrasts, function(x) {which(x$qvalue <= 0.1 & x$ks.pval > 0.1)}) 

lapply(sign_metabolites, function(x) {metabolite_names[x]})

lmer_res_list$contrasts$`Healthy - IBS-C`

```


```{r plotting SCFA acetate, message=FALSE, warning=FALSE}

sign_met_rows <- sign_metabolites$`Healthy - IBS-C`

comp <- lmer_res_list$contrasts
setwd("/Users/m210320/Dropbox/IBS/2019_06_files/R_RM/figure_output")
for (i in 1:length(sign_met_rows)) {
  ind <- sign_met_rows[i]
  data <- as.numeric(log10(biop_mbx_data[ind,]))
  q.vals_test <- c(comp$`Healthy - IBS-C`$qvalue[ind], comp$`Healthy - IBS-D`$qvalue[ind])
  make_boxplot_figure(data, biop_mbx_meta$Cohort, plottitle = metabolite_names[sign_met_rows[i]], ylable = "log10(mg/gram stool)", project = "metabolomics Biopsy SCFA", left_margin=4, add_significance=TRUE, q.vals=q.vals_test, file_type="png",  symbol=T)
}


```


```{r metabolomics serum tryptophan biopsy times, message=FALSE, warning=FALSE}

setwd("/Users/m210320/Dropbox/IBS/Metabonomics/data/Serum_tryptophan")
sample_name <- "serum tryptophan.xlsx"

ser_trp_df <- as.data.frame(read_excel(sample_name, sheet=1), stringsAsFactors=F)
names(ser_trp_df)[2] <- "Cohort"

ser_trp_df_data <- t(ser_trp_df[,-c(1:2)])
ser_trp_df_meta <- ser_trp_df[,c(1:2)]

metabolite_names <- rownames(ser_trp_df_data)
Cohort <- ser_trp_df_meta$Cohort

#values below 0 replaced by 1's
ser_trp_df_data[which(ser_trp_df_data < 0)] <- 1
ser_trp_df_data[which(ser_trp_df_data == 0)] <- 1

#no info on subjectID
#use lm

lm_res_list <- list()
for (i in 1:nrow(ser_trp_df_data)) {
  x <- as.numeric(log10(ser_trp_df_data[i,]))
  fit <- lm(x ~ Cohort)
  lm_res_list[[i]] <- get_contrasts(fit, adjust = "none")
}
names(lm_res_list) <- metabolite_names
contrasts_adj_list <- linear_models_adjusting_pvals(lm_res_list)
sign_met_rows <- lapply(contrasts_adj_list, function(x) {which(x$qvalue <= 0.1)})$`Healthy - IBS-C`

#9, tryptophan for H - IBS-C at q <0.1.
#tryptophan higher in D, lower in serum in C

#contrasts_adj_list$`Healthy - IBS-C`[9,]


#------------------------------------------
#repeat for IBS-C with Wilcox test, tryptophan 0.12. Probably won't hold when splitting up by timepoint. Do not use.
pvals <- c()
for (i in 1:length(metabolite_names)) {
  pvals[i] <- round(wilcox.test(as.numeric(log10(ser_trp_df_data[i,Cohort == "IBS-C"])),  as.numeric(log10(ser_trp_df_data[i,Cohort == "Healthy"])))$p.value,8)
}  
res_df_mean_C <- as.data.frame(cbind(metabolite_names, pvals))
res_df_mean_C$pvals <- as.numeric(as.character(res_df_mean_C$pvals))
res_df_mean_C$qvals <- p.adjust(res_df_mean_C$pvals, "fdr")




```


```{r plotting serum tryptophan, message=FALSE, warning=FALSE}

comp <- contrasts_adj_list
setwd("/Users/m210320/Dropbox/IBS/2019_06_files/R_RM/figure_output")
for (i in 1:length(sign_met_rows)) {
  ind <- sign_met_rows[i]
  data <- as.numeric(log10(ser_trp_df_data[ind,]))
  q.vals_test <- c(comp$`Healthy - IBS-C`$qvalue[ind], comp$`Healthy - IBS-D`$qvalue[ind])
  make_boxplot_figure(data, ser_trp_df_meta$Cohort, plottitle = metabolite_names[sign_met_rows[i]], ylable = "log10(relative abundance)", project = "metabolomics serum", left_margin=4, add_significance=TRUE, q.vals=q.vals_test, file_type="png", symbol=T)
}


```


```{r plotting data, message=FALSE, warning=FALSE}

par(mfrow=c(3,4))
for (i in 1:length(metabolite_names)) {
  par(mar=c(2,2.5,1.5,0))
  boxplot(as.numeric(ser_trp_df_data[i,]) ~ Cohort, varwidth=T, las=1, ylab="conc.", xlab="")
  title(metabolite_names[i], line=0.5)
}


```







