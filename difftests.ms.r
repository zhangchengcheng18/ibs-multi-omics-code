#Test for Differential Taxa

setwd("~/Dropbox/Mayo_postdoc/Tonya20190701_ibs_project-master/code")
source('stats.ms.r')

ALPHA <- 0.25

#all relative abundance data, remove first column with taxa names before doing stats
test.xs <- lapply(x_RA_collapsed_tax_list_woF_sub, function(xx) {t(xx[,c(-1)])})

########################################
#OR collapsed including flares to inspect differences. Metadata is identical
#test.xs <- lapply(x_RA_collapsed_tax_list_wF_sub, function(xx) {t(xx[,c(-1)])})

########################################

temp_cohorts <- mc$Cohort 

ixc.hc <- temp_cohorts == "H"
ixc.ibsc <- temp_cohorts == "C"
ixc.ibsd <- temp_cohorts == "D"
ixc.ibs <- temp_cohorts != "H"
test.ixs <- list('HC v. IBS'=ixc.hc | ixc.ibs, #c in name indicates without flares
                  'HC v. IBSD'=ixc.hc | ixc.ibsd,
                  'HC v. IBSC'=ixc.hc | ixc.ibsc)
plot_by <- c("IBS", "Cohort", "Cohort")
col_list <- list(cols_ibs, cols_dh, cols_ch, cols)

# run all combinations of tests
for(i in 1:length(test.xs)){
    x.name <- names(test.xs)[i]
    test.x <- test.xs[[i]]

    for(j in 1:length(test.ixs)){
        test.name <- names(test.ixs)[j]
        test.ix <- test.ixs[[j]]
        cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
        #differentiation.test function in stats.r script
        difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], mc[test.ix,plot_by[j]], parametric=FALSE)
        if(any(na.omit(difftest.np$qvalues <= ALPHA))){
            signif.ix <- which(difftest.np$qvalues <= ALPHA)
            signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
            res <- data.frame(Taxon=colnames(test.x)[signif.ix],
                              qvalue=round(difftest.np$qvalues[signif.ix],5),
                              pvalue=round(difftest.np$pvalues[signif.ix],5),
                              round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                              log2FC = round(difftest.np$log2FC[signif.ix],5))
            res <- res[order(abs(res$log2FC), decreasing = T),]
   
            sink(sprintf('diff_taxa/differential_abundance_%s_%s.txt',x.name, test.name))
            #cat(paste('q=',qval,' taxon: ',colnames(test.x)[k],' ks.test pval=',difftest.np$pvalues[k],'\n',sep=''))
            write.table(res,row.names=F,sep='\t',quote=F)
            sink()
            pdf(sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name),width=3,height=3)
            for(k in signif.ix){
              qval <- difftest.np$qvalues[k]
                working_mc <- subset(mc, test.ix)
                working_mc$vari <- data.transform(test.x)[test.ix,k]
                #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
                
                plot1 <- ggplot(working_mc, aes_string(x=plot_by[j], y= "vari")) +
                  geom_boxplot(outlier.shape = NA) +
                  geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
                  labs(x="", y= "Relative Abundance (sq rt)", title = colnames(test.x)[k]) +
                  theme(plot.title = element_text(size=8)) +
                  guides(fill=F, color=F) +
                  scale_y_continuous(trans='sqrt') +
                  scale_color_manual(values=col_list[[j]]) +
                  theme_cowplot(font_size = 10)
                plot(plot1)
            }
            dev.off()
            cat("see file\n")
        } else {
            cat("None\n")
        }
    }
}


######################################################################
#use non-parametric test on randomly drawn samples from the data
#first test time points individually

#do not include flares, m is the metadata structure. Do not select flares through selecting time points 0:6
ALPHA <- 0.25
timepoints <- c(0:6)
timepoints_names <- c(timepoints, "collapsed")
print_figures <- FALSE
#print_figures <- TRUE

res_overview_df_list <- list()
res_list_full <- list()

for (q in 1:length(x_RA_tax_list_sub)) {

comp_level <- names(x_RA_tax_list_sub)[q]
  
input_data <- x_RA_tax_list_sub[[q]][,-1]
collapsed_input_data <- t(x_RA_collapsed_tax_list_woF_sub[[q]][,-1])
collapsed_cohort <- mc$Cohort

microbiome_split_list <- sapply(timepoints, function(x) {t(input_data[,m$Timepoint_full == x])})
microbiome_split_list[[length(microbiome_split_list)+1]] <- collapsed_input_data
names(microbiome_split_list) <- timepoints_names
#make numeric in the for loop below
meta_split_list <- lapply(as.list(timepoints), function(x) {m[which(m$Timepoint_full == x),]})
meta_split_list[[length(meta_split_list)+1]] <- mc
names(meta_split_list) <- timepoints_names

test.ixs_list <- list()
for (i in 1:length(meta_split_list)) {
  temp_cohorts <- meta_split_list[[i]]$Cohort
  ixc.hc <- temp_cohorts == "H"
  ixc.ibsc <- temp_cohorts == "C"
  ixc.ibsd <- temp_cohorts == "D"
  ixc.ibs <- temp_cohorts != "H"
  test.ixs <- list('HC v. IBS'=ixc.hc | ixc.ibs, #c in name indicates without flares
                   'HC v. IBSD'=ixc.hc | ixc.ibsd,
                   'HC v. IBSC'=ixc.hc | ixc.ibsc)
  test.ixs_list[[i]] <- test.ixs

  meta_split_list[[i]]$IBS <- as.character(meta_split_list[[i]]$Cohort)
  meta_split_list[[i]]$IBS[meta_split_list[[i]]$IBS %in% c("C", "D")] <- "IBS"
  meta_split_list[[i]]$IBS <- as.factor(meta_split_list[[i]]$IBS)
}
plot_by <- c("IBS", "Cohort", "Cohort") #column names
col_list <- list(cols_ibs, cols_dh, cols_ch, cols)


names(test.ixs_list) <- timepoints_names
test.xs <- microbiome_split_list
#3 comparisons for all 7 time points
res_list <- list()
# run all combinations of tests for individual time point data
for(i in 1:length(test.xs)) {
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]
  meta_temp <- meta_split_list[[i]]
  test.ixs <- test.ixs_list[[i]]
  res_list_temp <- vector("list", 3)
  names(res_list_temp) <- names(test.ixs)
  comp_time <- timepoints_names[i]
  for (j in 1:length(test.ixs)){
    test.name <- names(test.ixs)[j]
    test.ix <- test.ixs[[j]]
    cat(sprintf('Significant q-values for %s %s, %s:\n', "timepoint", x.name, test.name))
    difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], meta_temp[test.ix,plot_by[j]], parametric=FALSE)
    if (any(na.omit(difftest.np$qvalues <= ALPHA))){
      signif.ix <- which(difftest.np$qvalues <= ALPHA)
      signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
      res <- data.frame(Taxon=colnames(test.x)[signif.ix],
                        qvalue=round(difftest.np$qvalues[signif.ix],5),
                        pvalue=round(difftest.np$pvalues[signif.ix],5),
                        round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[signif.ix],5))
      res <- res[order(abs(res$log2FC), decreasing = T),]
      
      if (print_figures) {
      cat(sprintf("%s significant hits\n", length(signif.ix)))
      
      sink(sprintf('diff_taxa/diff_abun_%s_%s_%s_%s.txt', comp_level, comp_time, x.name, test.name))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
      pdf(sprintf('diff_taxa/diff_abun_%s_%s_%s_%s.pdf', comp_level, comp_time, x.name, test.name),width=3,height=3)
      for (k in signif.ix){
        working_mc <- subset(meta_temp, test.ix)
        working_mc$vari <- data.transform(test.x)[test.ix,k]
        #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
        plot1 <- ggplot(working_mc, aes_string(x=plot_by[j], y= "vari")) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
          labs(x="", y= "Relative Abundance (sq rt)", title = colnames(test.x)[k]) +
          theme(plot.title = element_text(size=8)) +
          guides(fill=F, color=F) +
          scale_y_continuous(trans='sqrt') +
          scale_color_manual(values=col_list[[j]]) +
          theme_cowplot(font_size = 10)
        plot(plot1)
      }
      dev.off()
      }
      cat(i,j,"\n")
      res_list_temp[[j]] <- as.numeric(signif.ix)
    } else {
      cat("None\n")
      cat(i,j,"\n")
    }
  }
  res_list[[i]] <- res_list_temp
}
names(res_list) <- timepoints_names

res_list_length <- lapply(res_list, function(x) lapply(x, length))
res_overview_df <- as.data.frame(do.call(cbind, lapply(res_list_length, function(x) do.call(rbind, x))))
colnames(res_overview_df) <- timepoints_names

res_overview_df_list[[q]] <- res_overview_df
res_list_full[[q]] <- res_list
}
names(res_overview_df_list) <- names(x_RA_tax_list_sub)
names(res_list_full) <- names(x_RA_tax_list_sub)
#res_overview_df_list
res_overview_df <- do.call(rbind, res_overview_df_list)


######################################################################
#make into figure

temp_colors <- col_list[[4]][c(2,3,5,4)]

png(sprintf("1. number of sign taxa comparisons.png"), width=3.5*300, height=6.5*300, res=350)
par(mfrow=c(3,1))
for (j in 1:3) {
  temp_df <- as.data.frame(do.call(rbind, lapply(res_overview_df_list, function(x) {x[j,]})))
  comparison <- rownames(res_overview_df_list[[1]][j,])
  xlenght <- length(timepoints_names)
  #initite grid
  par(mar=c(3,4,2,1))
  plot(1:xlenght, seq(0, max(temp_df), length=xlenght), axes=F, xlab="", ylab="", pch="", ylim=c(0, max(temp_df)))
  axis(2, las=1, at=1:max(temp_df))
  mtext("number of signficant taxa", 2, line=2.5, cex=0.7)
  text(1:xlenght, par("usr")[3], labels=timepoints_names, srt=45, pos=1, xpd = TRUE)
  #i indicates split by taxonomic level
  for (i in 1:nrow(temp_df)) {
    points(1:xlenght, temp_df[i,], pch=16, col=temp_colors[i], cex=1.2)
  }
  #overplot 0 points
  points(1:xlenght, rep(0, xlenght), pch=16, col="white", cex=1.25)
  legend("topleft", legend=rownames(temp_df), cex=0.7, text.col =temp_colors, bty="n")
  title(paste(comparison, "<FDR 0.25"), line=0.5, cex.main=0.8)
  
  if (j == 1) {
    text_one <- rownames(x_RA_tax_list_sub$species)[res_list_full$species$`0`$`HC v. IBS`]
    text_two <- rownames(x_RA_tax_list_sub$species)[res_list_full$species$collapsed$`HC v. IBS`]
    text_col <- temp_colors[4]
    text(1, seq(3, 5, length.out=length(text_one)), text_one, col=text_col, cex=0.5, pos=4)
    text(8, seq(6, 14, length.out=length(text_two)), text_two, col=text_col, cex=0.5, pos=2)
  }
  if (j == 2) {
    text_one <- rownames(x_RA_tax_list_sub$phylum)[res_list_full$phylum$`5`$`HC v. IBSD`]
    text_two <- rownames(x_RA_tax_list_sub$phylum)[res_list_full$phylum$collapsed$`HC v. IBSD`]
    text_col <- temp_colors[1]
    text(5, seq(4, 5.5, length.out=length(text_one)), text_one, col=text_col, cex=0.5, pos=4)
    text(8.5, seq(6, 9, length.out=length(text_two)), text_two, col=text_col, cex=0.5, pos=2)
  }
  if (j == 3) {
    text_one <- rownames(x_RA_tax_list_sub$family)[res_list_full$family$`5`$`HC v. IBSC`]
    text_two <- rownames(x_RA_tax_list_sub$family)[res_list_full$family$collapsed$`HC v. IBSC`]
    text_col <- temp_colors[2]
    text(5, seq(2, 3, length.out=length(text_one)), text_one, col=text_col, cex=0.5, pos=4)
    text(8.5, seq(6, 9, length.out=length(text_two)), text_two, col=text_col, cex=0.5, pos=2)
  }
  
}
dev.off()


######################################################################
#names of different significant taxa per comparison 

#H - IBS species, 0 and collapsed


#HC - D phylum
rownames(x_RA_tax_list_sub$phylum)[res_list_full$phylum$`5`$`HC v. IBSD`]
rownames(x_RA_tax_list_sub$phylum)[res_list_full$phylum$collapsed$`HC v. IBSD`]

#HC - C family
rownames(x_RA_tax_list_sub$family)[res_list_full$family$`5`$`HC v. IBSC`]
rownames(x_RA_tax_list_sub$family)[res_list_full$family$collapsed$`HC v. IBSC`]



######################################################################
#Test for flare differences

ALPHA <- 0.15

test.xs <- lapply(x_RA_tax_list_sub, function(xx) {t(xx[,c(-1)])})
#lapply(test.xs, dim) #correct

# different group comparisons
IBS.flare <- m$IBS == "IBS" & !is.na(m$Flare)
IBS.Nflare <- m$IBS == "IBS" & is.na(m$Flare)
D.flare <- m$Cohort == "D" & !is.na(m$Flare)
D.Nflare <- m$Cohort == "D" & is.na(m$Flare)
C.flare <-  m$Cohort == "C" & !is.na(m$Flare)
C.Nflare <- m$Cohort == "C" & is.na(m$Flare)

test.ixs <- list('IBSflare v. IBSNF'= IBS.flare | IBS.Nflare,
                 'IBSDflare v. IBSDNF'=  D.flare | D.Nflare,
                 'IBSCflare v. IBSCNF'= C.flare | C.Nflare)

m$Flare <- as.character(m$Flare)
m[is.na(m$Flare),"Flare"] <- "Baseline"
m$Flare <- factor(m$Flare)

plot_by <- c("Flare", "Flare", "Flare")
col_list <- list(cols_flare, cols_flare, cols_flare)

# run all combinations of tests
for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]
  
  for(j in 1:length(test.ixs)){
    test.name <- names(test.ixs)[j]
    test.ix <- test.ixs[[j]]
    cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
    difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], m[test.ix,plot_by[j]], parametric=FALSE)
    if(any(difftest.np$qvalues <= ALPHA)){
      signif.ix <- which(difftest.np$qvalues <= ALPHA)
      signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
      res <- data.frame(Taxon=colnames(test.x)[signif.ix],
                        qvalue=round(difftest.np$qvalues[signif.ix],5),
                        pvalue=round(difftest.np$pvalues[signif.ix],5),
                        round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[signif.ix],5))
      res <- res[order(abs(res$log2FC), decreasing = T),]
      sink(sprintf('diff_taxa/differential_abundance_%s_%s.txt',x.name, test.name))
      #cat(paste('q=',qval,' taxon: ',colnames(test.x)[k],' ks.test pval=',difftest.np$pvalues[k],'\n',sep=''))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
      pdf(sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name),width=3,height=3)
      for(k in signif.ix){
        qval <- difftest.np$qvalues[k]
        working_m <- subset(m, test.ix)
        working_m$vari <- data.transform(test.x)[test.ix,k]
        #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
        plot1 <- ggplot(working_m, aes_string(x=plot_by[j], y= "vari")) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
          #theme(legend.position = 'bottom') + 
          labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
          theme(plot.title = element_text(size=10)) +
          guides(fill=F, color=F) +
          scale_y_continuous(trans='sqrt') +
          scale_color_manual(values=col_list[[j]]) +
          theme_cowplot(font_size = 10)
        plot(plot1)
      }
      dev.off()
      cat("see file\n")
    } else {
      cat("None\n")
    }
  }
}

######################################################################
#flare filtering data in order to compare mean data with the flare samples

ALPHA <- 0.1

#make new data and combine
flare_ind <- which(!is.na(m$Flare))

test.xs <- lapply(x_RA_tax_list_sub, function(xx) {t(xx[,c(-1)])}) #this is all data, with m as metadata
test.xs_flares <- lapply(test.xs, function(xx) {xx[flare_ind,]})

meta_flare <- m[flare_ind,c(11,68)]
meta_flare$Flare <- "Flare"

test.xs_collapsed <- lapply(x_RA_collapsed_tax_list_woF_sub, function(xx) {t(xx[,c(-1)])})
meta_collapsed <- mc[c(11,66)]
meta_collapsed$Flare <- NA

#combine new metadata
meta_combined <- rbind(meta_flare, meta_collapsed)

#combined data, flare first
test.xs <- list()
test.xs[[1]] <- rbind(test.xs_flares[[1]], test.xs_collapsed[[1]])
test.xs[[2]] <- rbind(test.xs_flares[[2]], test.xs_collapsed[[2]])
test.xs[[3]] <- rbind(test.xs_flares[[3]], test.xs_collapsed[[3]])
test.xs[[4]] <- rbind(test.xs_flares[[4]], test.xs_collapsed[[4]])

names(test.xs) <- names(test.xs_flares)
lapply(test.xs, dim)


# different group comparisons
IBS.flare <- meta_combined$IBS == "IBS" & !is.na(meta_combined$Flare)
IBS.Nflare <- meta_combined$IBS == "IBS" & is.na(meta_combined$Flare)
D.flare <- meta_combined$Cohort == "D" & !is.na(meta_combined$Flare)
D.Nflare <- meta_combined$Cohort == "D" & is.na(meta_combined$Flare)
C.flare <-  meta_combined$Cohort == "C" & !is.na(meta_combined$Flare)
C.Nflare <- meta_combined$Cohort == "C" & is.na(meta_combined$Flare)


test.ixs <- list('IBSflare v. IBSNF'= IBS.flare | IBS.Nflare,
                 'IBSDflare v. IBSDNF'=  D.flare | D.Nflare,
                 'IBSCflare v. IBSCNF'= C.flare | C.Nflare)

meta_combined$Flare <- as.character(meta_combined$Flare)
meta_combined[is.na(meta_combined$Flare),"Flare"] <- "Baseline"
meta_combined$Flare <- factor(meta_combined$Flare)

plot_by <- c("Flare", "Flare", "Flare")
col_list <- list(cols_flare, cols_flare, cols_flare)

# run all combinations of tests
for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]
  
  for(j in 1:length(test.ixs)){
    test.name <- names(test.ixs)[j]
    test.ix <- test.ixs[[j]]
    cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
    difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], meta_combined[test.ix,plot_by[j]], parametric=FALSE)
    if(any(na.omit(difftest.np$qvalues <= ALPHA))){
      signif.ix <- which(difftest.np$qvalues <= ALPHA)
      signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
      res <- data.frame(Taxon=colnames(test.x)[signif.ix],
                        qvalue=round(difftest.np$qvalues[signif.ix],5),
                        pvalue=round(difftest.np$pvalues[signif.ix],5),
                        round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[signif.ix],5))
      res <- res[order(abs(res$log2FC), decreasing = T),]
      sink(sprintf('diff_taxa/differential_abundance_%s_%s.txt',x.name, test.name))
      #cat(paste('q=',qval,' taxon: ',colnames(test.x)[k],' ks.test pval=',difftest.np$pvalues[k],'\n',sep=''))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
      pdf(sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name),width=4,height=4)
      for(k in signif.ix){
        qval <- difftest.np$qvalues[k]
        working_m <- subset(meta_combined, test.ix)
        working_m$vari <- data.transform(test.x)[test.ix,k]
        #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
        plot1 <- ggplot(working_m, aes_string(x=plot_by[j], y= "vari")) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
          #theme(legend.position = 'bottom') + 
          labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
          theme(plot.title = element_text(size=10)) +
          guides(fill=F, color=F) +
          scale_y_continuous(trans='sqrt') +
          scale_color_manual(values=col_list[[j]]) +
          theme_cowplot(font_size = 10)
        plot(plot1)
      }
      dev.off()
      cat("see file\n")
    } else {
      cat("None\n")
    }
  }
}

#checking Lachnospiraceae species
#colnames(test.xs$species)[grep("Lachnospiraceae", colnames(test.xs$species))]

######################################################################
#symptom severity, pcoa, and bcdi differences

ALPHA <- 0.05

test.xs <- lapply(x_RA_tax_list_sub, function(xx) {t(xx[,c(-1)])}) #this is all data, with m as metadata
lapply(test.xs, dim)

# different group comparisons
IBS.SSSmild <- m$IBS == "IBS" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
IBS.SSSsevere <- m$IBS == "IBS" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
D.SSSmild <- m$Cohort == "D" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
D.SSSsevere <- m$Cohort == "D" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
C.SSSmild <-  m$Cohort == "C" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
C.SSSsevere <- m$Cohort == "C" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
H.SSSmild <-  m$Cohort == "H" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
H.SSSsevere <- m$Cohort == "H" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300

table(D.SSSmild);table(D.SSSsevere);table(C.SSSmild);table(C.SSSsevere);table(H.SSSmild);table(H.SSSsevere)

m$SSS_binary <- "none_reported"
m$SSS_binary[m$IBS_symptom_severity_1_500 < 300] <- "mild"
m$SSS_binary[m$IBS_symptom_severity_1_500 >= 300] <- "severe"

#and the microbiome pcoa extreme samples and irregularity
IBS.PCOAnormal <- m$IBS == "IBS" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "normal"
IBS.PCOAextreme <- m$IBS == "IBS" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "extreme"
D.PCOAnormal <- m$Cohort == "D" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "normal"
D.PCOAextreme <- m$Cohort == "D" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "extreme"
C.PCOAnormal <-  m$Cohort == "C" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "normal"
C.PCOAextreme <- m$Cohort == "C" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "extreme"

IBS.BCDInormal <- m$IBS == "IBS" & !is.na(m$BCDI) & m$BCDI == "normal"
IBS.BCDIirreg <- m$IBS == "IBS" & !is.na(m$BCDI) & m$BCDI == "irregular"
D.BCDInormal <- m$Cohort == "D" & !is.na(m$BCDI) & m$BCDI == "normal"
D.BCDIirreg <- m$Cohort == "D" & !is.na(m$BCDI) & m$BCDI == "irregular"
C.BCDInormal <-  m$Cohort == "C" & !is.na(m$BCDI) & m$BCDI == "normal"
C.BCDIirreg <- m$Cohort == "C" & !is.na(m$BCDI) & m$BCDI == "irregular"


test.ixs <- list('IBS.ssn_severe vs IBS.sss_mild' = IBS.SSSsevere | IBS.SSSmild,
                 'IBSD_ssn_severe v. IBSD_ssn_mild'=  D.SSSsevere | D.SSSmild,
                 'IBSCssn_severe v. IBSC_ssn_mild'= C.SSSsevere | C.SSSmild,
                 'IBS.pcoa.extr vs IBS.pcoa.norm' = IBS.PCOAextreme | IBS.PCOAnormal,
                 'IBSD.pcoa.extr v. IBSD.pcoa.norm'=  D.PCOAextreme | D.PCOAnormal,
                 'IBSC.pcoa.extr v. IBSC.pcoa.norm'= C.PCOAextreme | C.PCOAnormal,
                 'IBS.bcdi.irreg vs IBS.bcdi.norm' = IBS.BCDIirreg | IBS.BCDInormal,
                 'IBSD.bcdi.irreg v. IBSD.bcdi.norm'=  D.BCDIirreg | D.BCDInormal,
                 'IBSC.bcdi.irreg v. IBSC.bcdi.norm'= C.BCDIirreg | C.BCDInormal)

plot_by <- c("SSS_binary", "SSS_binary", "SSS_binary", "extreme_pcoa", "extreme_pcoa", "extreme_pcoa", "BCDI", "BCDI", "BCDI")
col_list <- list(rev(cols_flare), rev(cols_flare), rev(cols_flare), cols_flare, cols_flare, cols_flare, cols_flare, cols_flare, cols_flare)


# run all combinations of tests
for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]
  
  for(j in 1:length(test.ixs)){
    test.name <- names(test.ixs)[j]
    test.ix <- test.ixs[[j]]
    cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
    difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], m[test.ix,plot_by[j]], parametric=FALSE)
    if(any(na.omit(difftest.np$qvalues <= ALPHA))){
      signif.ix <- which(difftest.np$qvalues <= ALPHA)
      signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
      res <- data.frame(Taxon=colnames(test.x)[signif.ix],
                        qvalue=round(difftest.np$qvalues[signif.ix],5),
                        pvalue=round(difftest.np$pvalues[signif.ix],5),
                        round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[signif.ix],5))
      res <- res[order(abs(res$log2FC), decreasing = T),]
      sink(sprintf('diff_taxa/differential_abundance_%s_%s.txt',x.name, test.name))
      #cat(paste('q=',qval,' taxon: ',colnames(test.x)[k],' ks.test pval=',difftest.np$pvalues[k],'\n',sep=''))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
      pdf(sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name),width=4,height=4)
      for(k in signif.ix){
        qval <- difftest.np$qvalues[k]
        working_m <- subset(m, test.ix)
        working_m$vari <- data.transform(test.x)[test.ix,k]
        #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
        plot1 <- ggplot(working_m, aes_string(x=plot_by[j], y= "vari")) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
          #theme(legend.position = 'bottom') + 
          labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
          theme(plot.title = element_text(size=10)) +
          guides(fill=F, color=F) +
          scale_y_continuous(trans='sqrt') +
          scale_color_manual(values=col_list[[j]]) +
          theme_cowplot(font_size = 10)
        plot(plot1)
      }
      dev.off()
      cat("see file\n")
    } else {
      cat("None\n")
    }
  }
}

######################################################################
#check bias in symptom severity, extreme_pcoa, and bcdi

#all biased but by different main subjects

temp_table <- table(m$study_id, m$extreme_pcoa)
temp_table[rev(order(temp_table[,1])),] #biased; 10007567 has 7 samples, 10007581 has 4, 10007584 has 3

temp_table <- table(m$study_id, m$BCDI)
temp_table[rev(order(temp_table[,2])),] #biased; 10007606 has 7, 10007548 has 7, 10007547 has 6, 10007607 has 5

temp_table <- table(m$study_id, m$SSS_binary)
temp_table[rev(order(temp_table[,3])),] #biased; 10007555 has 7, 10007544 has 7, 10007584 has 5


######################################################################
#test for biopsy taxa differences
#separate for both timepoints

#all relative abundance data, remove first column with taxa names before doing stats

ALPHA <- 0.25
timepoints <- c(1,2)
timepoints_names <- timepoints

timepoints_names <- c(timepoints)
#print_figures <- FALSE
print_figures <- TRUE

for (q in 1:length(xbio_RA_tax_list_sub)) {
  
  comp_level <- names(xbio_RA_tax_list_sub)[q]
  input_data <- xbio_RA_tax_list_sub[[q]][,-1]
 
  microbiome_split_list <- sapply(timepoints, function(x) {t(input_data[,m_bio$Timepoint == x])})
  names(microbiome_split_list) <- timepoints_names
  #make numeric in the for loop below
  meta_split_list <- lapply(as.list(timepoints), function(x) {m_bio[which(m_bio$Timepoint == x),]})
  names(meta_split_list) <- timepoints_names
  
  test.ixs_list <- list()
  for (i in 1:length(meta_split_list)) {
    temp_cohorts <- meta_split_list[[i]]$Cohort
    ixc.hc <- temp_cohorts == "H"
    ixc.ibsc <- temp_cohorts == "C"
    ixc.ibsd <- temp_cohorts == "D"
    ixc.ibs <- temp_cohorts != "H"
    test.ixs <- list('HC v. IBS'=ixc.hc | ixc.ibs, #c in name indicates without flares
                     'HC v. IBSD'=ixc.hc | ixc.ibsd,
                     'HC v. IBSC'=ixc.hc | ixc.ibsc)
    test.ixs_list[[i]] <- test.ixs
    
    meta_split_list[[i]]$IBS <- as.character(meta_split_list[[i]]$Cohort)
    meta_split_list[[i]]$IBS[meta_split_list[[i]]$IBS %in% c("C", "D")] <- "IBS"
    meta_split_list[[i]]$IBS <- as.factor(meta_split_list[[i]]$IBS)
  }
  plot_by <- c("IBS", "Cohort", "Cohort") #column names
  col_list <- list(cols_ibs, cols_dh, cols_ch, cols)
  
  
  names(test.ixs_list) <- timepoints_names
  test.xs <- microbiome_split_list
  #3 comparisons for both time points
  res_list <- list()
  # run all combinations of tests for individual time point data
  for(i in 1:length(test.xs)) {
    x.name <- names(test.xs)[i]
    test.x <- test.xs[[i]]
    meta_temp <- meta_split_list[[i]]
    test.ixs <- test.ixs_list[[i]]
    res_list_temp <- vector("list", 3)
    names(res_list_temp) <- names(test.ixs)
    comp_time <- timepoints_names[i]
    for (j in 1:length(test.ixs)){
      test.name <- names(test.ixs)[j]
      test.ix <- test.ixs[[j]]
      cat(sprintf('Significant q-values for %s %s, %s:\n', "timepoint", x.name, test.name))
      difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], meta_temp[test.ix,plot_by[j]], parametric=FALSE)
      if (any(na.omit(difftest.np$qvalues <= ALPHA))){
        signif.ix <- which(difftest.np$qvalues <= ALPHA)
        signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
        res <- data.frame(Taxon=colnames(test.x)[signif.ix],
                          qvalue=round(difftest.np$qvalues[signif.ix],5),
                          pvalue=round(difftest.np$pvalues[signif.ix],5),
                          round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                          log2FC = round(difftest.np$log2FC[signif.ix],5))
        if (print_figures) {
          cat(sprintf("%s significant hits\n", length(signif.ix)))
          
          sink(sprintf('diff_taxa/diff_abun_%s_%s_%s_%s.txt', comp_level, comp_time, x.name, test.name))
          write.table(res,row.names=F,sep='\t',quote=F)
          sink()
          pdf(sprintf('diff_taxa/diff_abun_%s_%s_%s_%s.pdf', comp_level, comp_time, x.name, test.name),width=3,height=3)
          for (k in signif.ix){
            working_mc <- subset(meta_temp, test.ix)
            working_mc$vari <- data.transform(test.x)[test.ix,k]
            #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
            plot1 <- ggplot(working_mc, aes_string(x=plot_by[j], y= "vari")) +
              geom_boxplot(outlier.shape = NA) +
              geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
              labs(x="", y= "Relative Abundance (sq rt)", title = colnames(test.x)[k]) +
              theme(plot.title = element_text(size=8)) +
              guides(fill=F, color=F) +
              scale_y_continuous(trans='sqrt') +
              scale_color_manual(values=col_list[[j]]) +
              theme_cowplot(font_size = 10)
            plot(plot1)
          }
          dev.off()
        }
        cat(i,j,"\n")
        res_list_temp[[j]] <- as.numeric(signif.ix)
      } else {
        cat("None\n")
        cat(i,j,"\n")
      }
    }
    res_list[[i]] <- res_list_temp
  }
  names(res_list) <- timepoints_names
}



######################################################################
#files manually moved to respective folders

#######################################################################
#Heatmap of diff flare species level

setwd("~/Dropbox/Mayo_postdoc/Tonya20190701_ibs_project-master/code/diff_taxa/flare differences")
df <- read.table("differential_abundance_species_IBSDflare v. IBSDNF.txt", sep="\t", header = T)

heat_mat <- as.matrix(df[,c(4,5)])
col_palette <- colorRampPalette(c("white", "black"))(10)

#add log2 fold change column?

png("IBS-D heatmap.png", width=1000, height=3000, res=300)
par(mar=c(2,2,2,2))
heatmap.2(sqrt(heat_mat), dendrogram="none", trace="none", hline="", Rowv=T, symm=FALSE, 
          col=col_palette, symbreaks=FALSE, labCol=c("Baseline", "Flare"), labRow= df$Taxon,
          srtCol=45, margins=c(4,12), cexCol=0.8, cexRow=0.6, lhei=c(3,3), lwid=c(1,2.2), key=F)
dev.off()


#IBS-C
df <- read.table("differential_abundance_species_IBSCflare v. IBSCNF.txt", sep="\t", header = T)
heat_mat <- as.matrix(df[,c(4,5)])
col_palette <- colorRampPalette(c("white", "black"))(10)

png("IBS-C heatmap.png", width=1000, height=3000, res=300)
par(mar=c(2,2,2,2))
heatmap.2(sqrt(rbind(heat_mat,heat_mat)), dendrogram="none", trace="none", hline="", Rowv=T, symm=FALSE, 
          col=col_palette, symbreaks=FALSE, labCol=c("Baseline", "Flare"), labRow= df$Taxon,
          srtCol=45, margins=c(4,12), cexCol=0.8, cexRow=0.6, lhei=c(3,0.4), lwid=c(1,2.2), key=F)
dev.off()



df <- read.table("differential_abundance_species_IBSflare v. IBSNF.txt", sep="\t", header = T)
heat_mat <- as.matrix(df[,c(4,5)])
col_palette <- colorRampPalette(c("white", "black"))(10)
#check old heatmap code, add log2 fold change column
png("IBS heatmap.png", width=1000, height=3000, res=300)
par(mar=c(2,2,2,2))
heatmap.2(sqrt(heat_mat), dendrogram="none", trace="none", hline="", Rowv=T, symm=FALSE, 
          col=col_palette, symbreaks=FALSE, labCol=c("Baseline", "Flare"), labRow= df$Taxon,
          srtCol=45, margins=c(4,12), cexCol=0.8, cexRow=0.6, lhei=c(3,0.5), lwid=c(1,2.2), key=F)
dev.off()



######################################################################
#Test for Symptom severity differences

ALPHA <- 0.1

test.xs <- lapply(x_RA_tax_list_sub, function(xx) {t(xx[,c(-1)])})

# different group comparisons
IBS.SSSmild <- m$IBS == "IBS" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
IBS.SSSsevere <- m$IBS == "IBS" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
D.SSSmild <- m$Cohort == "D" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
D.SSSsevere <- m$Cohort == "D" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
C.SSSmild <-  m$Cohort == "C" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
C.SSSsevere <- m$Cohort == "C" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
H.SSSmild <-  m$Cohort == "H" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
H.SSSsevere <- m$Cohort == "H" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300

table(D.SSSmild);table(D.SSSsevere);table(C.SSSmild);table(C.SSSsevere);table(H.SSSmild);table(H.SSSsevere)

m$SSS_binary <- "none_reported"
m$SSS_binary[m$IBS_symptom_severity_1_500 < 300] <- "mild"
m$SSS_binary[m$IBS_symptom_severity_1_500 >= 300] <- "severe"

test.ixs <- list('IBS.ssn_severe vs IBS.sss_mild' = IBS.SSSsevere | IBS.SSSmild,
                 'IBSD_ssn_severe v. IBSD_ssn_mild'=  D.SSSsevere | D.SSSmild,
                 'IBSCssn_severe v. IBSC_ssn_mild'= C.SSSsevere | C.SSSmild)

plot_by <- c("SSS_binary", "SSS_binary", "SSS_binary")
col_list <- list(rev(cols_flare), rev(cols_flare), rev(cols_flare))

# run all combinations of tests
for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]
  
  for(k in 1:ncol(test.x)){
    temp_ind <- which(k_map$V1 == colnames(test.x)[k])
    if(length(temp_ind > 0)){
      colnames(test.x)[k] <- paste(k_map$V5[temp_ind[1]])
    }
  }
  
  for(j in 1:length(test.ixs)){
    test.name <- names(test.ixs)[j]
    test.ix <- test.ixs[[j]]
    cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
    difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], m[test.ix,plot_by[j]], parametric=FALSE)
    difftest.np$qvalues[is.na(difftest.np$qvalues)] <- 1
    if(any(difftest.np$qvalues <= ALPHA)){
      signif.ix <- which(difftest.np$qvalues <= ALPHA)
      signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
      res <- data.frame(Module=colnames(test.x)[signif.ix],
                        qvalue=round(difftest.np$qvalues[signif.ix],5),
                        pvalue=round(difftest.np$pvalues[signif.ix],5),
                        round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[signif.ix],5))
      res <- res[order(abs(res$log2FC), decreasing = T),]
      sink(sprintf('diff_taxa/differential_abundance_%s_%s.txt',x.name, test.name))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
      pdf(sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name),width=4,height=4)
      for(k in signif.ix){
        qval <- difftest.np$qvalues[k]
        working_m <- subset(m, test.ix)
        working_m$vari <- data.transform(test.x)[test.ix,k]
        #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
        plot1 <- ggplot(working_m, aes_string(x=plot_by[j], y= "vari")) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
          #theme(legend.position = 'bottom') + 
          labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
          scale_x_discrete(labels=c("SSS <300", "SSS >300")) +
          theme(plot.title = element_text(size=10)) +
          guides(fill=F, color=F) +
          scale_y_continuous(trans='sqrt') +
          scale_color_manual(values=col_list[[j]][c(2,1)]) +
          theme_cowplot(font_size = 10)
        plot(plot1)
      }
      dev.off()
      cat("see file\n")
    } else {
      cat("None\n")
    }
  }
}

######################################################################






