
#initiates colors
setwd("~/Dropbox/Mayo_postdoc/Gitlab_mayo/general_functions/")
source("initiate_IBS_plotting.R")

setwd("~/Dropbox/Mayo_postdoc/Tonya20190701_ibs_project-master/code")

cat('Load files\n')
source("../code/load.essentials.ms.r") #loads data and metadata and gives complete timelines, initiates folders for output

cat('Sample Overview\n')
source("../code/sample_overview.ms.r") #creates timeline plot and table with study characteristics

#run Rmarkdown scripts here
setwd("/Users/m210320/Dropbox/IBS/2019_06_files/R_RM")

#source(calling_Rmarkdown_scripts.R)
rmarkdown::render('IBS_sample_numbers_fig_metadata_analysis.Rmd')
rmarkdown::render('IBS_microbiome_Bray_Curtis_v6_RM.Rmd')
rmarkdown::render('IBS_tryptophan_metabolites.Rmd')
rmarkdown::render('IBS_bile_acids_metabolites.Rmd')


#loads selected samples in vitro experiments file: #outdated can be removed
#setwd("../../in vitro experiment")
require(openxlsx)
#iv_samples <- as.data.frame(read_excel("selected samples for in vitro.xlsx", sheet="Sheet1"), stringsAsFactors=F)
#iv_samples_2 <- as.data.frame(read_excel("selected samples for in vitro.xlsx", sheet="Sheet2"), stringsAsFactors=F)


setwd("~/Dropbox/Mayo_postdoc/Tonya20190701_ibs_project-master/code")

cat('Alpha Diversity tests:\n')
source("../code/alpha.div.ms.r") #done

#cat('Alpha Diversity tests (rarefied):\n') 
#source("../code/alpha.div.rare.r") #done but not included

cat('Alpha Diversity tests (modules):\n')
source("../code/alpha.div.modules.ms.r") #done

cat('Alpha Diversity tests (kegg):\n')
source("../code/alpha.div.kegg.ms.r") #done

cat('Beta Diversity tests:\n')
source("../code/beta.div.ms.r") #done

#cat('Beta Diversity tests (rarefied):\n')
#source("../code/beta.div.rare.r")

cat('Beta Diversity tests (modules):\n')
source("../code/beta.div.modules.ms.r") #done

cat('Beta Diversity tests (modules):\n')
source("../code/beta.div.kegg.ms.r") #done


cat('Differential abundance tests:\n')
source('../code/difftests.ms.r') #done

cat('Differential abundance tests:\n')
source('../code/difftests.modules.ms.r') #done

cat('Differential abundance tests:\n')
source('../code/difftests.kegg.ms.r') #done

