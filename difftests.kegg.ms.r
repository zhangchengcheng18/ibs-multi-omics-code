#Test for differentiation across groups


#only keep kegg modules that are in the map file, no useful interpretation possible otherwise

#-------------------------------------
#optional, not implemented
#subset k_map with modules that might have different K term but same metadata
k_map_unique <- k_map %>% distinct(V2, V3, V4, V5, .keep_all = TRUE)
#dim(k_map) #to 1493 from 2326
k_map_unique[grep("xanthine", tolower(k_map_unique$V5)),]

#baiF bile salt hydrolase, K01442 is the actual enzyme though. Not significant in the tables
#https://www.genome.jp/dbget-bin/www_bget?ko:K01442 

k_map_unique[grep("bile", tolower(k_map_unique$V5)),]
k_map_unique[grep("K01442", k_map_unique$V1),]

k_map_unique[grep("K01593", k_map_unique$V1),] #tryptophan decarboxylase 
#not present after filtering, is this filtering too strict?

#butanoate metabolism modules:
#thiolase EC 2.3.1.9 (THL)
#β-hydroxybutyryl-CoA dehydrogenase (BHCD; EC 1.1.1.35)
#crotonase (CRO; EC 4.2.1.17) 
#butyryl-CoA dehydrogenase (BCD; EC 1.3.99.2)
#Electron-Transfer Flavoprotein α and β sub-units (ETFα and ETF β; E.C. 1.5.5.1)
#butyrate kinase (BK; EC 2.7.2.7)
#butyryl-coA acetyl-coA transferase (ButCoA; EC 2.8.3.8

EC_terms_butyrate <- c("2.3.1.9", "1.1.1.35", "4.2.1.17", "1.3.99.2", "1.5.5.1", "2.7.2.7", "2.8.3.8")



#-------------------------------------
#collapsed stool data:

ALPHA <- 0.1

test.df <- keggc[,which(colnames(keggc) %in% k_map$V1)] #dim 75 2184
test.xs <- list(otu=test.df)

# different group comparisons
test.ixs <- list('HC v. IBS'=ixc.hc | ixc.ibs,
                 'HC v. IBSD'=ixc.hc | ixc.ibsd,
                 'HC v. IBSC'=ixc.hc | ixc.ibsc)
plot_by <- c("IBS", "Cohort", "Cohort")
col_list <- list(cols_ibs, cols_dh, cols_ch)


for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]

  # run all combinations of tests; combine colnames K number and full name
  for(k in 1:ncol(test.x)){
    temp_ind <- which(k_map$V1 == colnames(test.x)[k])
    if(length(temp_ind > 0)){
      colnames(test.x)[k] <- paste(k_map$V1[temp_ind[1]], k_map$V5[temp_ind[1]])
    }
  }
    for(j in 1:length(test.ixs)){
        test.name <- names(test.ixs)[j]
        test.ix <- test.ixs[[j]]
        cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
        difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], mc[test.ix,plot_by[j]], parametric=FALSE)
        difftest.np$qvalues[is.na(difftest.np$qvalues)] <- 1
        
        #inspecting specific abundances
        #sel_rows <- grep("xanthine", tolower(colnames(test.x))) 
        #sel_rows <- grep("bile", tolower(colnames(test.x)))
        #sel_rows <- grep("choloylglycine hydrolase", tolower(colnames(test.x)))
        sel_rows <- unlist(sapply("xanthine", function(x) {grep(x, colnames(test.x))}))
        
        if(length(sel_rows) > 0) {
          res <- data.frame(Module=colnames(test.x)[sel_rows],
                            qvalue=round(difftest.np$qvalues[sel_rows],5),
                            pvalue=round(difftest.np$pvalues[sel_rows],5),
                            round(difftest.np$classwise.means[sel_rows,,drop=F],5),
                            log2FC = round(difftest.np$log2FC[sel_rows],5))
          #remove hashtags when wanting to look at files
          sink(sprintf('KEGG/diff_kegg/sel_modules_abundance_%s_%s.txt',x.name, test.name))
          write.table(res,row.names=F,sep='\t',quote=F)
          sink()
        }
        ###
        
        if(any(difftest.np$qvalues <= ALPHA)){
            signif.ix <- which(difftest.np$qvalues <= ALPHA)
            signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
            res <- data.frame(Module=colnames(test.x)[signif.ix],
                              qvalue=round(difftest.np$qvalues[signif.ix],5),
                              pvalue=round(difftest.np$pvalues[signif.ix],5),
                              round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                              log2FC = round(difftest.np$log2FC[signif.ix],5))
            res <- res[order(abs(res$log2FC), decreasing = T),]
            sink(sprintf('KEGG/diff_kegg/differential_abundance_%s_%s.txt',x.name, test.name))
            write.table(res,row.names=F,sep='\t',quote=F)
            sink()
            pdf(sprintf('KEGG/diff_kegg/differential_abundance_%s_%s.pdf',x.name, test.name),width=4,height=4)
            for(k in signif.ix){
              qval <- difftest.np$qvalues[k]
                working_mc <- subset(mc, test.ix)
                working_mc$vari <- data.transform(test.x)[test.ix,k]
       
                plot1 <- ggplot(working_mc, aes_string(x=plot_by[j], y= "vari")) +
                  geom_boxplot(outlier.shape = NA) +
                  geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
                  labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
                  theme(plot.title = element_text(size=10)) +
                  guides(fill=F, color=F) +
                  scale_y_continuous(trans='sqrt') +
                  scale_color_manual(values=col_list[[j]]) +
                  theme_cowplot(font_size = 10)
                plot(plot1)
            }
            dev.off()
            cat("see file\n")
        } else {
            cat("None\n")
        }
    }
}


######################################################################
#Test for flare differences

ALPHA <- 0.15

test.df <- kegg[,which(colnames(kegg) %in% k_map$V1)] #dim 474 2184
test.xs <- list(otu=test.df)

# different group comparisons
IBS.flare <- m$IBS == "IBS" & !is.na(m$Flare)
IBS.Nflare <- m$IBS == "IBS" & is.na(m$Flare)
D.flare <- m$Cohort == "D" & !is.na(m$Flare)
D.Nflare <- m$Cohort == "D" & is.na(m$Flare)
C.flare <-  m$Cohort == "C" & !is.na(m$Flare)
C.Nflare <- m$Cohort == "C" & is.na(m$Flare)

test.ixs <- list('IBSflare v. IBSNF'= IBS.flare | IBS.Nflare,
                  'IBSDflare v. IBSDNF'=  D.flare | D.Nflare,
                 'IBSCflare v. IBSCNF'= C.flare | C.Nflare)

m$Flare <- as.character(m$Flare)
m$Flare[is.na(m$Flare)] <- "Baseline"
m$Flare <- factor(m$Flare)

plot_by <- c("Flare", "Flare", "Flare")
col_list <- list(cols_flare, cols_flare, cols_flare)

# run all combinations of tests
for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]
  
  for(k in 1:ncol(test.x)){
    temp_ind <- which(k_map$V1 == colnames(test.x)[k])
    if(length(temp_ind > 0)){
      colnames(test.x)[k] <- paste(k_map$V1[temp_ind[1]], k_map$V5[temp_ind[1]])
    }
  }
  
  for(j in 1:length(test.ixs)){
    test.name <- names(test.ixs)[j]
    test.ix <- test.ixs[[j]]
    cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
    difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], m[test.ix,plot_by[j]], parametric=FALSE)
    difftest.np$qvalues[is.na(difftest.np$qvalues)] <- 1
    
    #inspecting specific abundances
    #sel_rows <- grep("xanthine", tolower(colnames(test.x))) #xanthine dehydrogenase modules: 
    #higher in C flares at 0.17 q-value, and below 0.15 for IBS-D 
    #sel_rows <- grep("bile", tolower(colnames(test.x))) #higher in D flares, 
    #this is a C. scindens enzyme involved in BA dehydroxylation https://www.genome.jp/dbget-bin/www_bget?ec:2.8.3.25
    #sel_rows <- grep("choloylglycine hydrolase", tolower(colnames(test.x))) #nothing
    sel_rows <- unlist(sapply("xanthine", function(x) {grep(x, colnames(test.x))}))
    if(length(sel_rows) > 0) {
      res <- data.frame(Module=colnames(test.x)[sel_rows],
                        qvalue=round(difftest.np$qvalues[sel_rows],5),
                        pvalue=round(difftest.np$pvalues[sel_rows],5),
                        round(difftest.np$classwise.means[sel_rows,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[sel_rows],5))
      #remove hashtags when wanting to look at files
      sink(sprintf('KEGG/diff_kegg/sel_modules_abundance_%s_%s.txt',x.name, test.name))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
    }
    ###
    
    if(any(difftest.np$qvalues <= ALPHA)){
      signif.ix <- which(difftest.np$qvalues <= ALPHA)
      signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
      res <- data.frame(Module=colnames(test.x)[signif.ix],
                        qvalue=round(difftest.np$qvalues[signif.ix],5),
                        pvalue=round(difftest.np$pvalues[signif.ix],5),
                        round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[signif.ix],5))
      res <- res[order(abs(res$log2FC), decreasing = T),]
      
      sink(sprintf('KEGG/diff_kegg/differential_abundance_%s_%s.txt',x.name, test.name))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
      pdf(sprintf('KEGG/diff_kegg/differential_abundance_%s_%s.pdf',x.name, test.name),width=4,height=4)
      for(k in signif.ix){
        qval <- difftest.np$qvalues[k]
        working_m <- subset(m, test.ix)
        working_m$vari <- data.transform(test.x)[test.ix,k]
        #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
        plot1 <- ggplot(working_m, aes_string(x=plot_by[j], y= "vari")) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
          #theme(legend.position = 'bottom') + 
          labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
          theme(plot.title = element_text(size=10)) +
          guides(fill=F, color=F) +
          scale_y_continuous(trans='sqrt') +
          scale_color_manual(values=col_list[[j]]) +
          theme_cowplot(font_size = 10)
        plot(plot1)
      }
      dev.off()
      cat("see file\n")
    } else {
      cat("None\n")
    }
  }
}

######################################################################
#Test for Symptom severity differences

ALPHA <- 0.1

test.df <- kegg[,which(colnames(kegg) %in% k_map$V1)] #dim 474 2184
test.xs <- list(otu=test.df)

# different group comparisons
IBS.SSSmild <- m$IBS == "IBS" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
IBS.SSSsevere <- m$IBS == "IBS" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
D.SSSmild <- m$Cohort == "D" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
D.SSSsevere <- m$Cohort == "D" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
C.SSSmild <-  m$Cohort == "C" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
C.SSSsevere <- m$Cohort == "C" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
H.SSSmild <-  m$Cohort == "H" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
H.SSSsevere <- m$Cohort == "H" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300

table(D.SSSmild);table(D.SSSsevere);table(C.SSSmild);table(C.SSSsevere);table(H.SSSmild);table(H.SSSsevere)

m$SSS_binary <- "none_reported"
m$SSS_binary[m$IBS_symptom_severity_1_500 < 300] <- "mild"
m$SSS_binary[m$IBS_symptom_severity_1_500 >= 300] <- "severe"

test.ixs <- list('IBS.ssn_severe vs IBS.sss_mild' = IBS.SSSsevere | IBS.SSSmild,
                  'IBSD_ssn_severe v. IBSD_ssn_mild'=  D.SSSsevere | D.SSSmild,
                 'IBSCssn_severe v. IBSC_ssn_mild'= C.SSSsevere | C.SSSmild)

plot_by <- c("SSS_binary", "SSS_binary", "SSS_binary")
col_list <- list(rev(cols_flare), rev(cols_flare), rev(cols_flare))

# run all combinations of tests
for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]
  
  for(k in 1:ncol(test.x)){
    temp_ind <- which(k_map$V1 == colnames(test.x)[k])
    if(length(temp_ind > 0)){
      colnames(test.x)[k] <- paste(k_map$V1[temp_ind[1]], k_map$V5[temp_ind[1]])
    }
  }
  
  for(j in 1:length(test.ixs)){
    test.name <- names(test.ixs)[j]
    test.ix <- test.ixs[[j]]
    cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
    difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], m[test.ix,plot_by[j]], parametric=FALSE)
    difftest.np$qvalues[is.na(difftest.np$qvalues)] <- 1
    if(any(difftest.np$qvalues <= ALPHA)){
      signif.ix <- which(difftest.np$qvalues <= ALPHA)
      signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
      res <- data.frame(Module=colnames(test.x)[signif.ix],
                        qvalue=round(difftest.np$qvalues[signif.ix],5),
                        pvalue=round(difftest.np$pvalues[signif.ix],5),
                        round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[signif.ix],5))
      res <- res[order(abs(res$log2FC), decreasing = T),]
      sink(sprintf('KEGG/diff_kegg/differential_abundance_%s_%s.txt',x.name, test.name))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
      pdf(sprintf('KEGG/diff_kegg/differential_abundance_%s_%s.pdf',x.name, test.name),width=4,height=4)
      for(k in signif.ix){
        qval <- difftest.np$qvalues[k]
        working_m <- subset(m, test.ix)
        working_m$vari <- data.transform(test.x)[test.ix,k]
        #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
        plot1 <- ggplot(working_m, aes_string(x=plot_by[j], y= "vari")) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
          #theme(legend.position = 'bottom') + 
          labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
          scale_x_discrete(labels=c("SSS <300", "SSS >300")) +
          theme(plot.title = element_text(size=10)) +
          guides(fill=F, color=F) +
          scale_y_continuous(trans='sqrt') +
          scale_color_manual(values=col_list[[j]][c(2,1)]) +
          theme_cowplot(font_size = 10)
        plot(plot1)
      }
      dev.off()
      cat("see file\n")
    } else {
      cat("None\n")
    }
  }
}

######################################################################
######################################################################
#Test for differences with outlying microbiome samples

ALPHA <- 0.05

test.df <- kegg[,which(colnames(kegg) %in% k_map$V1)] #dim 474 2184
test.xs <- list(otu=test.df)

m$cohort_extremes <- paste(m$Cohort, m$extreme_pcoa, sep="_")
m$cohort_BCDI <- paste(m$Cohort, m$BCDI, sep="_")

# different group comparisons
IBS.PCOAnormal <- m$IBS == "IBS" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "normal"
IBS.PCOAextreme <- m$IBS == "IBS" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "extreme"
D.PCOAnormal <- m$Cohort == "D" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "normal"
D.PCOAextreme <- m$Cohort == "D" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "extreme"
C.PCOAnormal <-  m$Cohort == "C" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "normal"
C.PCOAextreme <- m$Cohort == "C" & !is.na(m$extreme_pcoa) & m$extreme_pcoa == "extreme"

IBS.BCDInormal <- m$IBS == "IBS" & !is.na(m$BCDI) & m$BCDI == "normal"
IBS.BCDIirreg <- m$IBS == "IBS" & !is.na(m$BCDI) & m$BCDI == "irregular"
D.BCDInormal <- m$Cohort == "D" & !is.na(m$BCDI) & m$BCDI == "normal"
D.BCDIirreg <- m$Cohort == "D" & !is.na(m$BCDI) & m$BCDI == "irregular"
C.BCDInormal <-  m$Cohort == "C" & !is.na(m$BCDI) & m$BCDI == "normal"
C.BCDIirreg <- m$Cohort == "C" & !is.na(m$BCDI) & m$BCDI == "irregular"

table(IBS.PCOAnormal);table(IBS.PCOAextreme);table(D.PCOAnormal);table(D.PCOAextreme);table(C.PCOAnormal);table(C.PCOAextreme)
table(IBS.BCDInormal);table(IBS.BCDIirreg);table(D.BCDInormal);table(D.BCDIirreg);table(C.BCDInormal);table(C.BCDIirreg)

test.ixs <- list('IBS.pcoa.extr vs IBS.pcoa.norm' = IBS.PCOAextreme | IBS.PCOAnormal,
                 'IBSD.pcoa.extr v. IBSD.pcoa.norm'=  D.PCOAextreme | D.PCOAnormal,
                 'IBSC.pcoa.extr v. IBSC.pcoa.norm'= C.PCOAextreme | C.PCOAnormal,
                 'IBS.bcdi.irreg vs IBS.bcdi.norm' = IBS.BCDIirreg | IBS.BCDInormal,
                 'IBSD.bcdi.irreg v. IBSD.bcdi.norm'=  D.BCDIirreg | D.BCDInormal,
                 'IBSC.bcdi.irreg v. IBSC.bcdi.norm'= C.BCDIirreg | C.BCDInormal)

plot_by <- c("extreme_pcoa", "extreme_pcoa", "extreme_pcoa", "BCDI", "BCDI", "BCDI")
col_list <- list(cols_flare, cols_flare, cols_flare, cols_flare, cols_flare, cols_flare)

# run all combinations of tests
for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]
  
  for(k in 1:ncol(test.x)){
    temp_ind <- which(k_map$V1 == colnames(test.x)[k])
    if(length(temp_ind > 0)){
      colnames(test.x)[k] <- paste(k_map$V1[temp_ind[1]], k_map$V5[temp_ind[1]])
    }
  }
  
  for(j in 1:length(test.ixs)){
    test.name <- names(test.ixs)[j]
    test.ix <- test.ixs[[j]]
    cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
    difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], m[test.ix,plot_by[j]], parametric=FALSE)
    difftest.np$qvalues[is.na(difftest.np$qvalues)] <- 1
    if(any(difftest.np$qvalues <= ALPHA)){
      #adapted with fold change cutoff
      signif.ix <- which(difftest.np$qvalues <= ALPHA & (abs(difftest.np$log2FC) > 1))
      signif.ix <- signif.ix[order(difftest.np$qvalues[signif.ix])]
      res <- data.frame(Module=colnames(test.x)[signif.ix],
                        qvalue=round(difftest.np$qvalues[signif.ix],5),
                        pvalue=round(difftest.np$pvalues[signif.ix],5),
                        round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[signif.ix],5))
      res <- res[order(abs(res$log2FC), decreasing = T),]
      sink(sprintf('KEGG/diff_kegg/differential_abundance_%s_%s.txt',x.name, test.name))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
      pdf(sprintf('KEGG/diff_kegg/differential_abundance_%s_%s.pdf',x.name, test.name),width=4,height=4)
      for(k in signif.ix){
        qval <- difftest.np$qvalues[k]
        working_m <- subset(m, test.ix)
        working_m$vari <- data.transform(test.x)[test.ix,k]
        #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
        plot1 <- ggplot(working_m, aes_string(x=plot_by[j], y= "vari")) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
          #theme(legend.position = 'bottom') + 
          labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
          scale_x_discrete(labels=c("irregular", "normal")) +
          theme(plot.title = element_text(size=10)) +
          guides(fill=F, color=F) +
          scale_y_continuous(trans='sqrt') +
          scale_color_manual(values=col_list[[j]][c(2,1)]) +
          theme_cowplot(font_size = 10)
        plot(plot1)
      }
      dev.off()
      cat("see file\n")
    } else {
      cat("None\n")
    }
  }
}



######################################################################
#figure of relevant modules for hypoxanthine, manually selected from first sheet, and for all

require(readxl)

addErrorBars_horz <- function( x, y, err, ... ){ 	arrows( x+err, y, x-err, y, angle=90, code=3, length=.02, ...) }
se <- function(x) sqrt(var(na.omit(x))/length(na.omit(x)))
sel_colors <- c("#D95F02", "#0072B2", "#666666")  #C D H, e.g. for microbiome, from initiate_IBS_plotting file
sel_colors_table <- col2rgb(sel_colors) 
sel_colors_transp <- adjustcolor(sel_colors, alpha.f = 0.7)


sample_name <- "KEGG/diff_kegg/combined_module_201910.xlsx"
#tm for tryp metabolites
comb_mod <- as.data.frame(read_excel(sample_name, sheet="HC v. IBSC", skip=0), stringsAsFactors=F)

mod_to_plot <- comb_mod$Module[!is.na(comb_mod$nr)]

test.df <- keggc[,which(colnames(keggc) %in% k_map$V1)] #dim 75 2184, #averaged data
kos_temp <- colnames(test.df)

for (i in 1:length(kos_temp)) {
  temp_ind <- which(k_map$V1 == kos_temp[i]) 
  colnames(test.df)[i] <- paste(k_map$V1[temp_ind[1]], k_map$V5[temp_ind[1]])
}
ind_to_plot <- which(colnames(test.df) %in% mod_to_plot)

test.df_sel <- test.df[,ind_to_plot]
#add columns with data for more modules, xanthine and hypoxanthine phosphoribosyltransferase 
row_to_add <- grep("xanthine phosphoribosyltransferase", colnames(test.df))[1]
test.df_sel <- cbind(test.df_sel, test.df[,row_to_add])
colnames(test.df_sel)[ncol(test.df_sel)] <- colnames(test.df)[row_to_add]

#fold change wrt to mean of HC
fc_mat <- test.df_sel
for (i in 1:ncol(test.df_sel)) {
  fc_mat[,i] <-  log2(test.df_sel[,i] / colMeans(test.df_sel[mc$Cohort == "H",])[i])
}
rownames(fc_mat) <- mc$Cohort


######################################
png("module overview figure selected C.png", width=5*300, height=2.5*300, res=300)
par(mar=c(3,0,1,14))
par(mgp = c(0, 0.3, 0))

#initiate grid
xgrid <- c(-8:10)
ygrid <- c(0, ncol(fc_mat))

plot(xgrid, xgrid, ylim=c(ygrid), axes=F, ylab="", xlab="", pch="")
par(lend=2) #straight ends of lines
abline(v=0, col=sel_colors[3])
axis(1, cex.axis=0.7)

fc_mat_c <- fc_mat[rownames(fc_mat) == "C",] 
fc_mat_c <- fc_mat_c[,order(apply(fc_mat_c, 2, median))]

#for C
for (i in 1:ncol(fc_mat)) {
  data <- fc_mat_c[,i]
  points(data, rep(i, times=length(data)), pch=16, col=sel_colors_transp[1], cex=0.7)
  se(data)
  addErrorBars_horz(median(data), i, sd(data))
  text(8, i, colnames(fc_mat_c)[i], cex=0.6, pos=4, xpd=T)
  segments(median(data), i+0.2,median(data), i-0.2, lwd=1.4)
}
mtext("module log2(fold change) vs. HC", side = 1, line=1.5, cex=0.7)

dev.off()



######################################
#same for D as illustration
png("module overview figure selected D.png", width=5*300, height=2.5*300, res=300)
par(mar=c(3,1,1,14))

#initiate grid
xgrid <- c(-10:10)
ygrid <- c(0, ncol(fc_mat))

plot(xgrid, xgrid, ylim=c(ygrid), axes=F, ylab="", xlab="", pch="")
par(lend=2) #straight ends of lines
abline(v=0, col=sel_colors[3])
axis(1, cex.axis=0.7)

fc_mat_c <- fc_mat[rownames(fc_mat) == "D",] 
fc_mat_c <- fc_mat_c[,order(apply(fc_mat_c, 2, median))]

#for C
for (i in 1:ncol(fc_mat)) {
  data <- fc_mat_c[,i]
  points(data, rep(i, times=length(data)), pch=16, col=sel_colors_transp[2], cex=0.7)
  se(data)
  addErrorBars_horz(median(data), i, sd(data))
  text(8, i, colnames(fc_mat_c)[i], cex=0.6, pos=4, xpd=T)
  segments(median(data), i+0.2,median(data), i-0.2, lwd=1.4)
}
mtext("module log2(fold change) compared to HC", side = 1, line=2, cex=0.7)

dev.off()

######################################
#same for all significant ones for C for supplementary figure

mod_to_plot <- comb_mod$Module
test.df <- keggc[,which(colnames(keggc) %in% k_map$V1)] #dim 75 2184, #averaged data
kos_temp <- colnames(test.df)

for (i in 1:length(kos_temp)) {
  temp_ind <- which(k_map$V1 == kos_temp[i]) 
  colnames(test.df)[i] <- paste(k_map$V1[temp_ind[1]], k_map$V5[temp_ind[1]])
}
ind_to_plot <- which(colnames(test.df) %in% mod_to_plot)

test.df_sel <- test.df[,ind_to_plot]

#fold change wrt to mean of HC
fc_mat <- test.df_sel
for (i in 1:ncol(test.df_sel)) {
  fc_mat[,i] <-  log2(test.df_sel[,i] / colMeans(test.df_sel[mc$Cohort == "H",])[i])
}
rownames(fc_mat) <- mc$Cohort


######################################
png("module overview figure all C.png", width=5*300, height=9*300, res=300)
par(mar=c(3,1,1,14))

#initiate grid
xgrid <- c(-10:10)
ygrid <- c(0, ncol(fc_mat))

plot(xgrid, xgrid, ylim=c(ygrid), axes=F, ylab="", xlab="", pch="")
par(lend=2) #straight ends of lines
abline(v=0, col=sel_colors[3])
axis(1, cex.axis=0.7)

fc_mat_c <- fc_mat[rownames(fc_mat) == "C",] 
fc_mat_c <- fc_mat_c[,order(apply(fc_mat_c, 2, median))]

#for C
for (i in 1:ncol(fc_mat)) {
  data <- fc_mat_c[,i]
  points(data, rep(i, times=length(data)), pch=16, col=sel_colors_transp[1], cex=0.7)
  se(data)
  addErrorBars_horz(median(data), i, sd(data))
  text(8, i, colnames(fc_mat_c)[i], cex=0.6, pos=4, xpd=T)
  segments(median(data), i+0.2,median(data), i-0.2, lwd=1.4)
}
mtext("module log2(fold change) compared to HC", side = 1, line=2, cex=0.7)

dev.off()




######################################################################

png("modules heatmap IBS.png", width=1000, height=3000, res=300)
par(mar=c(2,2,2,2))
heatmap.2(heat_mat, dendrogram="none", trace="none", hline="", Rowv=T, symm=FALSE, 
          col=col_palette, symbreaks=T, labCol=colnames(df_fc), labRow= rownames(df_fc),
          srtCol=45, margins=c(6,13), cexCol=0.8, cexRow=0.6, lhei=c(3,3), lwid=c(1,2.2), key=T, 
          keysize=0.5, key.par = list(cex=0.6))
dev.off()

######################################################################
#boxplot of selected modules

#1.17.1.4 xanthine dehydrogenase/oxidase (XO)  
#2.4.2.22 xanthine phosphoribosyltransferase (XPRT) 

require(readxl)
sample_name <- "KEGG/diff_kegg/combined_module_txts.xlsx"
#tm for tryp metabolites
comb_mod <- as.data.frame(read_excel(sample_name, sheet=1, skip=0), stringsAsFactors=F)
mod_to_plot <- comb_mod$Module[!is.na(comb_mod$nr)]
mod_to_plot <- c(mod_to_plot, "2.4.2.22  xanthine phosphoribosyltransferase")

setwd("~/Dropbox/Mayo_postdoc/Gitlab_mayo/general_functions/")
source("initiate_IBS_plotting.R")
source("make_boxplot_figure.R")

test.df <- keggc[,which(colnames(keggc) %in% k_map$V1)] #dim 75 2184
kos_temp <- colnames(test.df)
full_names_temp <- kos_temp
for (i in 1:length(kos_temp)) {
  temp_ind <- which(k_map$V1 == kos_temp[i]) 
  full_names_temp[i] <- as.character(k_map$V5[temp_ind])
}

ind_to_plot <- which(full_names_temp %in% mod_to_plot)
#full_names_temp[ind_to_plot]

temp_cohorts <- as.character(mc$Cohort)
temp_cohorts[grep("D", temp_cohorts)] <- "IBS-D"
temp_cohorts[grep("C", temp_cohorts)] <- "IBS-C"
temp_cohorts[grep("H", temp_cohorts)] <- "Healthy"

setwd("/Users/m210320/Dropbox/IBS/2019_06_files/R_RM/figure_output")
for (i in 1:length(ind_to_plot)) {
  temp_data <- test.df[, ind_to_plot[i]]
  make_boxplot_figure(sqrt(temp_data), temp_cohorts, plottitle=paste(i, full_names_temp[ind_to_plot][i]), ylable = "sqrt(relative abundance)", xlable="", 
                    project = "KEGG GO", add_significance=F, 
                    add_significance_for_5=F, file_type="png",
                    horizontal=F, bottom_margin=4)
}

#---------------------------------------------------------------
#expand the XO one with statistics for main figure
#averaging was explored but 2:5 are very correlated (>0.9); does not matter which one is used

#ind_to_plot <- grep("1.17.1.4  xanthine dehydrogenase", full_names_temp)
#ind_to_plot #5 modules, try averaging and independently
#temp_data <- test.df[, ind_to_plot]
#temp_data_sum <- rowSums(temp_data)
#TukeyHSD(aov(temp_data_sum ~ temp_cohorts))
#cor(temp_data[,4], temp_data[,5]) #2:5 are very correlated; does not matter which one is used it seems
#head(temp_data)



test.df <- keggc[,which(colnames(keggc) %in% k_map$V1)] #dim 75 2184
temp_data <- test.df[,"K00087"]

setwd("/Users/m210320/Dropbox/IBS/2019_06_files/R_RM/figure_output")
q.vals_test <- c(0.09149, 0.80717)
make_boxplot_figure(sqrt(temp_data), temp_cohorts, plottitle= "K00087 1.17.1.4 XO", 
                    ylable = "sqrt(relative abundance)", xlable="", 
                    project = "KEGG GO", add_significance=T, q.vals=q.vals_test,
                    add_significance_for_5=F, file_type="png",
                    horizontal=F, bottom_margin=4, left_margin=4, symbol=T)



######################################################################
#summing up ADH enzyme abundance in all samples. How many ADH modules are there?
#use with flare comparison; not averaged

test.df <- kegg[,which(colnames(kegg) %in% k_map$V1)] #474 2184
kos_temp <- colnames(test.df)
full_names_temp <- kos_temp

adh_rows <- grep(" alcohol dehydrogenase", k_map$V5)
adh_Ks <- as.character(k_map$V1[adh_rows])

test.df_adh <- test.df[,adh_Ks]

adh_sum <- rowSums(test.df_adh)
flare_ind <- grep("Flare" , rownames(test.df_adh))

hist(adh_sum[-flare_ind], ylim=c(0,50))
hist(adh_sum[flare_ind], add=T, col="red")

#no clear pattern; different for specific modules?
par(mfrow=c(3,2)) 
par(mar=c(1,1,1,1))
for (i in 1:length(adh_Ks)) {
  hist(test.df_adh[-flare_ind, adh_Ks[i]], ylim=c(0,50), main=adh_Ks[i])
  hist(test.df_adh[flare_ind, adh_Ks[i]], add=T, col="red")
}

#K13954 and K13953 seem flare-associated
#it is specific modules that are elevated; not other ones. Unclear what the biological meaning of this is.
#correlate adh_sum with the detected ADH activity


#---------------------------------------------------------------
#read ADH data

setwd("/Users/m210320/Dropbox/IBS/in vitro experiment")

#ADH analyzed data
#navigate to folder and select file to read in data file
file_name <- "combined_ADH_assay_results.xlsx"
#adh_enzyme_df <- as.data.frame(read_excel(file_name, sheet="both exps combined"), stringsAsFactors=F)
adh_enzyme_df <- as.data.frame(read_excel(file_name, sheet="both exps combined subset"), stringsAsFactors=F)

head(adh_enzyme_df)

#add adh sum and different module abundances to this file
adh_enzyme_df$adh_module_sum <- "-"
adh_enzyme_df$K00001 <- "-"
adh_enzyme_df$K00121 <- "-"
adh_enzyme_df$K04072 <- "-"
adh_enzyme_df$K11440 <- "-"
adh_enzyme_df$K13953 <- "-"
adh_enzyme_df$K13954 <- "-"

for (i in 1:nrow(adh_enzyme_df)) {
  temp_row <- which(names(adh_sum) == adh_enzyme_df$ID[i])
  if (length(temp_row) > 0) {
    adh_enzyme_df[i,13:18] <- test.df_adh[temp_row,]
    adh_enzyme_df$adh_module_sum[i] <- as.numeric(adh_sum[temp_row])}
}
#adh_enzyme_df_trunc <- adh_enzyme_df[-which(adh_enzyme_df$adh_module_sum == "-"),]
adh_enzyme_df_trunc <- adh_enzyme_df #for the subsetted one

#correlating module abundance
columns_to_correlate <- 12:18
cor_list <- list()
cor_pval_list <- list()
sum_module_list <- list()

par(mfrow=c(4,2))
for (j in 1:length(columns_to_correlate)) {
  adh_act_vec <- as.numeric(adh_enzyme_df_trunc$`ADH activity (milliunits/mL)`)
  adh_module_abundance <- as.numeric(adh_enzyme_df_trunc[,columns_to_correlate[j]])
  cor_list[j] <- cor(adh_act_vec, adh_module_abundance, method="spearman")
  cor_pval_list[j] <- cor.test(adh_act_vec, adh_module_abundance, method="spearman")$p.value
  sum_module_list[j] <- sum(adh_module_abundance)
  par(mar=c(3,3,2,1))
  plot(adh_act_vec, adh_module_abundance, main=names(adh_enzyme_df_trunc)[columns_to_correlate[j]])
  
}
names(cor_list) <- names(adh_enzyme_df_trunc)[columns_to_correlate]
names(cor_pval_list) <- names(adh_enzyme_df_trunc)[columns_to_correlate]
names(sum_module_list) <- names(adh_enzyme_df_trunc)[columns_to_correlate]

cor_list
cor_pval_list
sum_module_list

#$adh_module_sum, cor 0.30, p.value 0.098, sum 2542724
#K00001, cor 0.46, p.value 0.01, sum 1479662
#K13953, cor 0.21, p.value 0.26, sum 182218 (8 times less than other module)

#checked K00001 for flares but there is no pattern; this one is determining the ADH activity

#correlation between K00001 and K13953
cor(test.df_adh[,"K00001"], test.df_adh[,"K13953"],  method="spearman") #-0.2695924
cor.test(test.df_adh[,"K00001"], test.df_adh[,"K13953"],  method="spearman") #-0.2695924

cor(test.df_adh[,"K00001"], test.df_adh[,"K04072"],  method="spearman") #-0.262089

#flare associated ones are correlated
cor(test.df_adh[,"K13953"], test.df_adh[,"K04072"],  method="spearman") #0.6728264 



######################################################################
#inspect in vitro manually selected samples for hypoxanthine and ADH on the non-averaged samples

#K04072 1.1.1.1  alcohol dehydrogenase
#K13953 1.1.1.1  alcohol dehydrogenase
#K13479 1.17.1.4  xanthine dehydrogenase
#K00087 1.17.1.4  xanthine dehydrogenase

sel_KO_enzymes <- c("K04072", "K13953", "K13479", "K00087")
sel_KO_enzyme_names <- c("K04072 ADH", "K13953 ADH", "K13479 XO", "K00087 XO")

#iv_samples; sheet 2 for selected ones

test.df <- kegg[,which(colnames(kegg) %in% k_map$V1)] #dim 474 2184
#temp_data <- test.df[which(rownames(test.df) %in% iv_samples_2$`ID on tube`), 
#                     which(colnames(test.df)  %in% sel_KO_enzymes)]
#dim(temp_data) #15  4

plot_groups <- sapply(rownames(temp_data), function(x) {iv_samples_2$group[iv_samples_2$`ID on tube` == x]})

require(beeswarm)

par(mfrow = c(2,2))
for (i in 1:length(sel_KO_enzymes)) {
  par(mar = c(3,8,2,1))
  beeswarm(temp_data[,i] ~ plot_groups, horizontal=T, las=1, ylab="", main=sel_KO_enzyme_names[i], pch=16)
}


###############################################################################################################
#sulfite reductase - BA association?
#Bile salt hydrolase - BA association?

#from Ridlon et al, 2016 https://doi.org/10.1080/19490976.2016.1150414
#Metabolism of taurine conjugated bile acids by gut microbes generates hydrogen sulfide, a genotoxic compound. 
#Thus, taurocholic acid has the potential to stimulate intestinal bacteria capable of converting taurine and cholic 
#acid to hydrogen sulfide and deoxycholic acid, a genotoxin and tumor-promoter, respectively.

#install.packages("compositions")
require(compositions)

#ordering k_map_unique by unique_id
k_map_unique <- k_map %>% distinct(V2, V3, V4, V5, .keep_all = TRUE)
#k_map_unique[grep("bile", tolower(k_map_unique$V5)),] #K15871 
#k_map_unique[grep("K01442", k_map_unique$V1),]
#k_map_unique[grep("sulfite", tolower(k_map_unique$V5)),] #K00380, K00392, K11180; K00392 is elevated in IBS-C

#non collapsed data
test.df <- kegg[,which(colnames(kegg) %in% k_map$V1)] #dim 474 2184
test.df_woF <- test.df[-which(m$Flare == "Flare"),]
m_woF <- m[-which(m$Flare == "Flare"),]
rownames(test.df_woF) <- m_woF$unique_id

#load IBS_bile_acids_metabolites.Rmd to get the ba_df_data_log10 data
dim(ba_df_data_log10)
ba_names <- tolower(rownames(ba_df_data_log10))
#get unique_id per sample
ba_unique_id <- paste(ba_df_meta$SubjectID, ba_df_meta$time_point, sep = "_")

#get the overlapping datasets and sort accordingly
ba_unique_id_sub <- ba_unique_id[ba_unique_id %in% rownames(test.df_woF)]
ba_df_data_log10_ord <- ba_df_data_log10[,order(ba_unique_id_sub)]
ba_unique_id_sub <- sort(ba_unique_id_sub)
names(ba_df_data_log10_ord) <- ba_unique_id_sub

#get the overlapping datasets for test.df_woF
test.df_woF_ord <- test.df_woF[which(rownames(test.df_woF) %in% ba_unique_id_sub),]

row_of_int <- which(colnames(test.df_woF_ord) == "K00392") #sulfite
row_of_int <- which(colnames(test.df_woF_ord) == "K15871") #BSH

x <- as.numeric(clr(test.df_woF_ord[,row_of_int])) #centered log ratio transform

cor_list <- vector(mode = "list", length = nrow(ba_df_data_log10_ord))
for (i in 1:nrow(ba_df_data_log10_ord)) {
  y <- as.numeric(ba_df_data_log10_ord[i,])
  cor_list[[i]][1] <- cor(x, y, method="spearman")
  cor_list[[i]][2] <- cor.test(x, y, method="spearman")$p.value
}
cor_df <- as.data.frame(do.call(rbind, cor_list), stringsAsFactors=F)
names(cor_df) <- c("cor", "p.val")
cor_df$fdr <- p.adjust(cor_df$p.val, "fdr")

cbind(ba_names,cor_df)

hist(test.df_woF_ord[,row_of_int], ylim=c(0,50), breaks=50)

#------------------------------------------------
#do correlation of sulfite reductase module only for IBS-C since it is elevated only in IBS-C

rows_cohort <- as.character(sapply(colnames(ba_df_data_log10_ord), function(x) m$Cohort[m$unique_id == x]))

row_of_int <- which(colnames(test.df_woF_ord) == "K00392") #sulfite
#row_of_int <- which(colnames(test.df_woF_ord) == "K15871") #BSH

x <- as.numeric(clr(test.df_woF_ord[rows_cohort == "C",row_of_int])) #centered log ratio transform

cor_list <- vector(mode = "list", length = nrow(ba_df_data_log10_ord))
for (i in 1:nrow(ba_df_data_log10_ord)) {
  y <- as.numeric(ba_df_data_log10_ord[i,rows_cohort == "C"])
  cor_list[[i]][1] <- cor(x, y, method="spearman")
  cor_list[[i]][2] <- cor.test(x, y, method="spearman")$p.value
}
cor_df <- as.data.frame(do.call(rbind, cor_list), stringsAsFactors=F)
names(cor_df) <- c("cor", "p.val")
cor_df$fdr <- p.adjust(cor_df$p.val, "fdr")

cbind(ba_names,cor_df)

#nothing else close except for; sulfite reductase module K00392 for C
#deoxycholic acid sulphate  0.193209228 0.02475449 0.2970538


###############################################################################################################
#XO module and extreme samples

test.df <- kegg[,which(colnames(kegg) %in% k_map$V1)] #dim 474 2184
test.df_woF <- test.df[-which(m$Flare == "Flare"),]
m_woF <- m[-which(m$Flare == "Flare"),]
rownames(test.df_woF) <- m_woF$unique_id

x <- test.df_woF[,"K00087"]

dim(test.df_woF)
dim(m_woF)

#add the cohort definition
cohort_temp <- as.character(sapply(names(x), function(z) m_woF$Cohort[m_woF$unique_id == z]))
subject_temp <- as.character(sapply(names(x), function(z) strsplit(z, "_")[[1]][1]))

uni_cohort_temp <- sapply(rev(sort(unique(subject_temp))), function(z) unique(m_woF$Cohort[m_woF$study_id == z]))

require(rafalib)
group <- as.fumeric(uni_cohort_temp)
sel_colors_temp_sort <- sel_colors[c(3,1,2)]

pdf("test.pdf", width=6, height=10)
par(mar=c(3,8,2,2))
boxplot(x ~ subject_temp, horizontal=T, las=1, cex.axis=0.7, xlab="", ylab="", )
dev.off()


#do these IBS-D samples have higher XO levels than the others?
m$cohort_extremes <- paste(m$Cohort, m$extreme_pcoa, sep="_")

table(m$cohort_extremes)

extreme_D_unique_ids <- m$unique_id[m$cohort_extremes == "D_extreme"]
extreme_C_unique_ids <- m$unique_id[m$cohort_extremes == "C_extreme"]

irregular_D_unique_ids <- m$unique_id[m$cohort_BCDI == "D_irregular"]
irregular_C_unique_ids <- m$unique_id[m$cohort_BCDI == "C_irregular"]


#------------------------------------------------
#D subjects with the highest levels

plot(density(x[cohort_temp == "D"]))
sort(x[cohort_temp == "D"])

H_95 <- quantile(x[cohort_temp == "H"], 0.95)

length(which(x[cohort_temp == "H"] > H_95)) #8
length(which(x[cohort_temp == "H"] > H_95)) / length(x[cohort_temp == "H"]) #5.33%


length(which(x[cohort_temp == "D"] > H_95)) #22
length(which(x[cohort_temp == "D"] > H_95)) / length(x[cohort_temp == "D"]) #12.9%

length(which(x[extreme_D_unique_ids] > H_95)) #2
length(which(x[extreme_D_unique_ids] > H_95)) / length(x[extreme_D_unique_ids]) #0.1%

length(which(x[irregular_D_unique_ids] > H_95)) #3
length(which(x[irregular_D_unique_ids] > H_95)) / length(x[irregular_D_unique_ids]) #11.5%



length(which(x[cohort_temp == "C"] > H_95)) #39
length(which(x[cohort_temp == "C"] > H_95)) / length(x[cohort_temp == "D"]) #22.9%

length(which(x[extreme_C_unique_ids] > H_95)) #1
length(which(x[extreme_C_unique_ids] > H_95)) / length(x[extreme_C_unique_ids]) #0.166%

length(which(x[irregular_C_unique_ids] > H_95)) #15
length(which(x[irregular_C_unique_ids] > H_95)) / length(x[irregular_C_unique_ids]) #30.6%


#------------------------------------------------

x <- test.df_woF[,"K00087"]

x_in <- x[m$cohort_extremes == "D_extreme"]
x_out <- x[m$cohort_extremes == "D_normal"]
boxplot(x_in, x_out) 
t.test(x_in, x_out)

hist(x_out, breaks=20)
hist(x_in, add=T, col="red", breaks=10)

#sign but lower for K00087; p 0.007 but only 6 samples
x_in <- x[m$cohort_extremes == "C_extreme"]
x_out <- x[m$cohort_extremes == "C_normal"]
boxplot(x_in, x_out) 
t.test(x_in, x_out)


#------------------------------------------------

m$cohort_BCDI <- paste(m$Cohort, m$BCDI, sep="_")

x_in <- x[m$cohort_BCDI == "D_irregular"]
x_out <- x[m$cohort_BCDI == "D_normal"]
boxplot(x_in, x_out) 
t.test(x_in, x_out)

hist(x_out, breaks=20)
hist(x_in, add=T, col="red", breaks=20)


x_in <- x[m$cohort_BCDI == "C_irregular"]
x_out <- x[m$cohort_BCDI == "C_normal"]
boxplot(x_in, x_out) 
t.test(x_in, x_out)


###############################################################################################################
#ADH module and extreme samples

#do these IBS-D samples have higher ADH levels than the others?
#K04072 1.1.1.1  alcohol dehydrogenase
#K13953 1.1.1.1  alcohol dehydrogenase

x <- test.df_woF[,"K04072"]
x <- test.df_woF[,"K13953"]

x_in <- x[m$cohort_extremes == "D_extreme"]
x_out <- x[m$cohort_extremes == "D_normal"]
boxplot(x_in, x_out) 
t.test(x_in, x_out)

hist(x_out, breaks=20)
hist(x_in, add=T, col="red")

#sign for K13953; but not the optimal test and only 6 samples so will not hold up
x_in <- x[m$cohort_extremes == "C_extreme"]
x_out <- x[m$cohort_extremes == "C_normal"]
boxplot(x_in, x_out) 
t.test(x_in, x_out)

hist(x_out, breaks=20)
hist(x_in, add=T, col="red")


#------------------------------------------------
m$cohort_BCDI <- paste(m$Cohort, m$BCDI, sep="_")

x_in <- x[m$cohort_BCDI == "D_irregular"]
x_out <- x[m$cohort_BCDI == "D_normal"]
boxplot(x_in, x_out) 
t.test(x_in, x_out)

hist(x_out, breaks=20)
hist(x_in, add=T, col="red", breaks=40)


x_in <- x[m$cohort_BCDI == "C_irregular"]
x_out <- x[m$cohort_BCDI == "C_normal"]
boxplot(x_in, x_out) 
t.test(x_in, x_out)

#------------------------------------------------
#symptom severity and irregularity; no significance

par(mar=c(3,6,3,3))
boxplot(m$IBS_symptom_severity_1_500 ~ m$cohort_extremes, horizontal=T, las=1, ylab="", varwidth=T)
TukeyHSD(aov(m$IBS_symptom_severity_1_500 ~ m$cohort_extremes))

par(mar=c(3,6,3,3))
boxplot(m$IBS_symptom_severity_1_500 ~ m$cohort_BCDI, horizontal=T, las=1, ylab="", varwidth=T)
TukeyHSD(aov(m$IBS_symptom_severity_1_500 ~ m$cohort_extremes))

###############################################################################################################
#kegg Z-scores using

subjectID_ids_kegg <- names(table(m$study_id))[table(m$study_id) > 4]

#subset it only for the modules of interest

input_data <- t(kegg[,c("K01442", "K13954")])

#k_map[which(k_map$V1 == c("K01442", "K15871", "K13954")[1]),]
#k_map[which(k_map$V1 == c("K01442", "K15871", "K13954")[2]),]
#k_map[which(k_map$V1 == c("K01442", "K15871", "K13954")[3]),]


kegg_metadata_list <- list()
subjectID_ids <- subject_ids_4timepoints
kegg_zscore_list <- list()
kegg_counts_list <- list()

for (i in 1:length(subjectID_ids_kegg)) {
  subject_cols <- which(m$study_id == subjectID_ids_kegg[i])
  kegg_names <- rownames(input_data[,subject_cols])
  temp_times <- as.numeric(m$Timepoint_full[subject_cols])
  t_zero_cols <- which(temp_times == min(temp_times))
  kegg_metadata_list[[i]] <- m[subject_cols,]
  
  #sort by time points
  temp_order <- order(as.numeric(temp_times))
  kegg_metadata_list[[i]] <- kegg_metadata_list[[i]][temp_order,]
  
  int_zscore_temp <- input_data[,subject_cols]
  temp_int <- input_data[,subject_cols]
  for (j in 1:nrow(int_zscore_temp)) {
    #Z-score changed to mean from median to prevent zeroes
    int_zscore_temp[j,] <- (as.numeric(temp_int[j,]) - mean(as.numeric(temp_int[j,]))) /  sd(as.numeric(temp_int[j,]))
  }
  kegg_zscore_list[[i]] <- data.frame(apply(int_zscore_temp[,temp_order], 2, function(x) as.numeric(as.character(x))))
  kegg_counts_list[[i]] <- temp_int
  colnames(kegg_zscore_list[[i]]) <- temp_times[temp_order]
  colnames(kegg_counts_list[[i]]) <- temp_times[temp_order]
  rownames(kegg_zscore_list[[i]]) <- kegg_names
}

names(kegg_metadata_list) <- subjectID_ids_kegg
names(kegg_zscore_list) <- subjectID_ids_kegg
names(kegg_counts_list) <- subjectID_ids_kegg

#testing meaningful intensities
#lapply(kegg_counts_list, rowSums)



